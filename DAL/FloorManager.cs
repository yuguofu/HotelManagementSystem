﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using Models;
using System.Configuration;

namespace DAL
{
    public class FloorManager
    {
        /// <summary>
        /// 判断 tb_floor是否存在
        /// </summary>
        /// <returns>存在大于0<returns>
        public bool IsFloorTableExits()
        {
            string sql = "select * from sqlite_master where type='table' and name='tb_floor'";
            SQLiteDataReader rd = SQLHelper.GetReader(sql);
            if (rd.HasRows)
            {
                rd.Close();
                return true;
            }
            else
            {
                rd.Close();
                return false;
            }
        }


        /// <summary>
        /// 创建楼层表
        /// </summary>
        /// <returns>返回受影响行数</returns>
        public int CreatTable()
        {
            string sql = "CREATE TABLE tb_floor (楼层 INT PRIMARY KEY UNIQUE NOT NULL,房间数 INT NOT NULL)";
            
            return SQLHelper.Update(sql);

        }

        /// <summary>
        /// 插入楼层房间数据
        /// </summary>
        /// <param name="floor">楼层</param>
        /// <param name="roomNum"></param>
        /// <returns></returns>
        public int InsertInfoIntoFloorTable(int floor, int roomNum)
        {
            string sql = String.Format("insert into tb_floor (楼层,房间数) values({0},{1})" , floor, roomNum);
            return SQLHelper.Update(sql);
        }


        /// <summary>
        /// 更新房间数
        /// </summary>
        /// <param name="floor"></param>
        /// <param name="roomNum"></param>
        /// <returns></returns>
        public int UpdateInfoOnFloorTable (int floor, int roomNum)
        {
            string sql = String.Format("update tb_floor set 房间数={0} where 楼层={1}",roomNum,floor);
            return SQLHelper.Update(sql);
        }


        public List<Models.Floor> GetFloorInfo()
        {
            string sql = "select * from tb_floor";
            SQLiteDataReader rd = SQLHelper.GetReader(sql);
            List<Models.Floor> list = new List<Models.Floor>();
            while (rd.Read())
            {
                list.Add(new Models.Floor { 
                    FloorNum=Convert.ToInt32(rd["楼层"]),
                    RoomNum=Convert.ToInt32(rd["房间数"]),
                    
                });
            }
            return list;
        }
    }
}
