﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using Models;

namespace DAL
{
    /// <summary>
    /// 预定管理 类
    /// </summary>
    public class ReserveMamager
    {
        /// <summary>
        /// 预定房间（插入预定表记录）
        /// </summary>
        /// <param name="reserve"></param>
        /// <returns></returns>
        public int ReserveRoom(Reserve reserve)
        {
            string sql = string.Format("insert into tb_reserve (身份证号,姓名,性别,预定房号,预定时间,预定时段) values('{0}','{1}','{2}','{3}','{4}','{5}')",reserve.IDCradNum,reserve.Name, reserve.Sex, reserve.ReserveRoomNum, reserve.ReserveTime, reserve.TimeSlice);
            return SQLHelper.Update(sql);
        }

        /// <summary>
        /// 删除预定记录
        /// </summary>
        /// <param name="roomNum"></param>
        /// <returns></returns>
        public int DeleReserve(string roomNum)
        {
            string sql = string.Format("delete from tb_reserve where 预定房号='{0}'",roomNum);
            return SQLHelper.Update(sql);
        }


        /// <summary>
        /// 获取预定信息
        /// </summary>
        /// <param name="roomNum"></param>
        /// <returns></returns>
        public List<Reserve> GetReservesInfo(string roomNum)
        {
            string sql = string.Format("select * from tb_reserve where 预定房号='{0}'",roomNum);
            SQLiteDataReader rd = SQLHelper.GetReader(sql);
            List<Reserve> list = new List<Reserve>();
            while (rd.Read())
            {
                list.Add(new Reserve { 
                    IDCradNum=rd["身份证号"].ToString(),
                    Name= rd["姓名"].ToString(),
                    Sex= rd["性别"].ToString(),
                    ReserveRoomNum= rd["预定房号"].ToString(),
                    ReserveTime= rd["预定时间"].ToString(),
                    TimeSlice= rd["预定时段"].ToString(),
                });
            }
            rd.Close();
            return list;
        }
    }
}
