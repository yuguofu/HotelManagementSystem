# winform酒店管理系统

#### 介绍
winform酒店管理系统,初学者作品，仅供学习交流。
.net framework4.6.2，sqlite3数据库


#### 安装教程
visual studio编辑，编译

#### 使用说明

visual studio编辑，编译

用户名 tingyu  密码 123456

#### 预览

![登录](https://images.gitee.com/uploads/images/2020/1119/202006_3981936b_5348320.png "屏幕截图.png")
![主界面](https://images.gitee.com/uploads/images/2020/1119/202038_4c66193f_5348320.png "屏幕截图.png")
![菜单操作](https://images.gitee.com/uploads/images/2020/1119/202220_ff156dc5_5348320.png "屏幕截图.png")