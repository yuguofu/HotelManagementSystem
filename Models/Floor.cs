﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class Floor
    {
        public int FloorNum { get; set; }
        public int RoomNum { get; set; }
    }
}
