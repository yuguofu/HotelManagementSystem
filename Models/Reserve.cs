﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class Reserve
    {
        public string IDCradNum { get; set; }
        public string Name { get; set; }
        public string Sex { get; set; }
        public string ReserveRoomNum { get; set; }
        public string ReserveTime { get; set; }
        public string TimeSlice { get; set; }
    }
}
