﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class CustomerHistory
    {
        //public int ID { get; set; }
        public string IDCardNum { get; set; }
        public string Name { get; set; }
        public string Sex { get; set; }
        public string RoomNum { get; set; } //入住房号
        public string StartTime { get; set; }    //开始时间
        public string EndTime { get; set; }      //结束时间
        public double ConsumptionAmount { get; set; }     //消费金额
        public string Remarks { get; set; }
    }
}
