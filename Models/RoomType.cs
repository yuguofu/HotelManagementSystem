﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class RoomType
    {
        public int TypeID { get; set; }
        public string TypeName { get; set; }
        public double UnitPrice { get; set; }

    }
}
