﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class User
    {
        public int ID { get; set; }
        public string UserName { get; set; }
        public string Pwd { get; set; }
        public int Power { get; set; }
        public string Remarks { get; set; }
    }
}
