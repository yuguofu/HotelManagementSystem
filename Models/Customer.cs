﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class Customer
    {
        //public int ID { get; set; }
        public string IDCardNum { get; set; }
        public string Name { get; set; }
        public string Sex { get; set; }
        public string RoomNum { get; set; } //入住房号
        public string BookedTime { get; set; }    //入住时间
        public int BookedDay { get; set; }      //预住时间
        public double Deposit { get; set; }     //押金
        public string Remarks { get; set; }
    }
}
