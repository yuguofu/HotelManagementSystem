﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class RoomInfo
    {
        public int FloorNum { get; set; }
        public string RoomNum { get; set; }
        public int RoomTypeID { get; set; }
        public string RoomTypeName { get; set; }
        public double UnitPrice { get; set; }
        public int RoomStateID { get; set; }
        public string RoomStateName { get; set; }
        public string Remarks { get; set; }
    }
}
