﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Models;

namespace 酒店管理系统___试
{
    public partial class FrmCheckedOut : Form
    {
        private DAL.RoomManager roomManager = new DAL.RoomManager();
        private DAL.CustomerManager customerManager = new DAL.CustomerManager();
        DAL.VIPManager vIPManager = new DAL.VIPManager();
        List<Customer> customerList;

        /// <summary>
        /// 窗口初始化
        /// </summary>
        /// <param name="room"></param>
        public FrmCheckedOut(UCRoomInfo.RoomInfo room)
        {
            InitializeComponent();

            List<Models.RoomInfo> roomInfoList = roomManager.GetRoomInfo(room.RoomNumber);
            customerList = customerManager.GetCustomerInfo(room.RoomNumber);

            double nitPrice = roomInfoList[0].UnitPrice;    //单价

            lblFloor.Text = room.RoomNumber.Split('-')[0] + "楼";    //楼层
            lblRoomNum.Text = room.RoomNumber;      //房号
            lblRoomType.Text = roomInfoList[0].RoomTypeName;    //类型
            lblUnitPrice.Text = nitPrice.ToString() + "/天";    //显示单价
            //顾客信息
            lblCustomerInfo.Text = "";
            if (customerList.Count > 0)
            {
                foreach (var item in customerList)
                {
                    lblCustomerInfo.Text += "身份证号:" + item.IDCardNum + " | 姓名:" + item.Name + " | 性别:" + item.Sex + " | 房号:" + item.RoomNum + " | 入住时间:" + item.BookedTime + " | 押金:" + item.Deposit + "\n";
                }
            }
            else
            {
                lblCustomerInfo.Text = "无";
            }
            lblBookedTime.Text = customerList[0].BookedTime;  //入住时间
            lblBookedDay.Text = customerList[0].BookedDay.ToString();  //预住天数
            lblDeposit.Text = customerList[0].Deposit.ToString();   //押金
            
            //VIP检查
            if (customerList.Count > 0)
            {
                foreach (var item in customerList)
                {
                    if (vIPManager.IsIncludeVIP(item))
                    {
                        lblVIP.Text = "是";
                        break;
                    }
                    else
                    {
                        lblVIP.Text = "否";
                    }
                }
            }
            
            //VIP折率（如9折 = 90% * 单价 = 0.9 * 单价）
            double discount = Convert.ToDouble(vIPManager.GetVIPDiscount()) * 0.1;
            
            //计算应付金额
               //计算所住天数
            DateTime bookedTime = Convert.ToDateTime(lblBookedTime.Text).Date;  //入住时间的0点
            Console.WriteLine("入住时间的0点  "+bookedTime);
            DateTime bookedTimeTomorrow12 = bookedTime.AddDays(1).AddHours(12); //后一天12点
            Console.WriteLine("入住后一天12点  " + bookedTimeTomorrow12);
            DateTime nowDay12 = DateTime.Now.Date.AddHours(12);     //今天12点
            DateTime nowDay18 = DateTime.Now.Date.AddHours(18);     //今天18点
            Console.WriteLine("今天12点  "+ nowDay12+ "    今天18点  " + nowDay18);
            TimeSpan ts1 = new TimeSpan(bookedTimeTomorrow12.Ticks);    //时间刻度1
            TimeSpan ts2 = new TimeSpan(nowDay12.Ticks);    //时间刻度2
            TimeSpan ts = ts2.Subtract(ts1).Duration();     //时间间隔绝对值
            Console.WriteLine("入住后一天12点到今天12间隔  "+ ts);

            double dayNum; //所住天数
            //判断是否过了今天12点
            if (DateTime.Compare(nowDay12,DateTime.Now) >0)  //未过12点
            {
                dayNum = ts.Days + 1;
            }
            else if (DateTime.Compare(nowDay18, DateTime.Now) > 0)  //未过18点
            {
                dayNum = ts.Days + 1 + 0.5;   //过了12点未过18点，将再收半天费用
            }
            else
            {
                dayNum = ts.Days + 1 + 1;   //过了18点，将再收1天费用
            }
            lblRealityDay.Text = dayNum.ToString();

            double payable;  //应付金额
            if (lblVIP.Text=="是")    //是VIP进行打折处理
            {
                payable = nitPrice * dayNum * discount; //单价*所住天数*折率
            }
            else
            {
                payable = nitPrice * dayNum; //单价*所住天数
            }

            Console.WriteLine("单价  " + nitPrice);
            Console.WriteLine("所住天数  " + dayNum);
            Console.WriteLine("折率  " + discount);

            lblPayable.Text = payable + " 元";
            txtPayAmount.Text = payable.ToString();
            lblRemarks.Text = roomInfoList[0].Remarks;
        }
        //取消退房
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //关闭窗口
        private void FrmCheckedOut_FormClosed(object sender, FormClosedEventArgs e)
        {
            DialogResult = DialogResult.Yes;
        }
        //确定退房
        private void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                //更新顾客历史表记录
                foreach (var item in customerList)
                {
                    customerManager.UpdateCustomerHistory(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), Convert.ToDouble(txtPayAmount.Text) / customerList.Count, item.IDCardNum);
                }
                //删除顾客表记录
                foreach (var item in customerList)
                {
                    customerManager.DelRecord(item.IDCardNum);
                }
                //更新房间状态表（打扫）
                int rs = roomManager.UpdateRoomState(customerList[0].RoomNum, 2);
                if (rs > 0)
                {
                    MessageBox.Show("退房结算成功！");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("失败！ "+ex.Message);
            }
        }
    }
}
