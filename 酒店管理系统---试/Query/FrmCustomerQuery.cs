﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace 酒店管理系统___试.Query
{
    public partial class FrmCustomerQuery : Form
    {
        private DAL.CustomerManager customerManager = new DAL.CustomerManager();
        public FrmCustomerQuery()
        {
            InitializeComponent();
        }

        private void FrmCustomerQuery_Load(object sender, EventArgs e)
        {
            DataSet ds = customerManager.GetAllCustomerInfoDataSet();
            dgvCustomerQueryResult.DataSource = ds.Tables[0];
        }



        private void btnQuery_Click(object sender, EventArgs e)
        {
            if (cmbQueryStype.SelectedIndex != -1)
            {
                switch (cmbQueryStype.Text)
                {
                    case "身份证号":
                        lblTip.Text = "";
                        lblTip.Visible = false;
                        dgvCustomerQueryResult.DataSource = null;
                        try
                        {
                            dgvCustomerQueryResult.DataSource = customerManager.GetCustomerInfoByIDCard(txtKeyWords.Text.Trim()).Tables[0];
                            lblTip.Text = "查询完毕 共" + dgvCustomerQueryResult.RowCount + "条记录";
                            lblTip.Visible = true;
                        }
                        catch (Exception ex)
                        {
                            lblTip.Text = "查询失败！";
                            lblTip.Visible = true;
                            MessageBox.Show(ex.Message);
                        }
                        break;
                    case "姓名":
                        lblTip.Text = "";
                        lblTip.Visible = false;
                        dgvCustomerQueryResult.DataSource = null;
                        try
                        {
                            dgvCustomerQueryResult.DataSource = customerManager.GetCustomerInfoByName(txtKeyWords.Text.Trim()).Tables[0];
                            lblTip.Text = "查询完毕 共" + dgvCustomerQueryResult.RowCount + "条记录";
                            lblTip.Visible = true;
                        }
                        catch (Exception ex)
                        {
                            lblTip.Text = "查询失败！";
                            lblTip.Visible = true;
                            MessageBox.Show(ex.Message);
                        }
                        break;
                    case "房号":
                        lblTip.Text = "";
                        lblTip.Visible = false;
                        dgvCustomerQueryResult.DataSource = null;
                        try
                        {
                            dgvCustomerQueryResult.DataSource = customerManager.GetCustomerInfoByRoomNum(txtKeyWords.Text.Trim()).Tables[0];
                            lblTip.Text = "查询完毕 共" + dgvCustomerQueryResult.RowCount + "条记录";
                            lblTip.Visible = true;
                        }
                        catch (Exception ex)
                        {
                            lblTip.Text = "查询失败！";
                            lblTip.Visible = true;
                            MessageBox.Show(ex.Message);
                        }
                        break;
                    case "查询全部":
                        FrmCustomerQuery_Load(sender, e);
                        break;
                }
            }
            else
            {
                MessageBox.Show("请选择查询方式！");
            }
        }

        private void cmbQueryStype_SelectionChangeCommitted(object sender, EventArgs e)
        {
            lblTip.Visible = false;
        }

        private void txtKeyWords_Enter(object sender, EventArgs e)
        {
            lblTip.Visible = false;
        }

        private void txtKeyWords_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                btnQuery_Click(sender, e);
            }
        }

        
    }
}
