﻿namespace 酒店管理系统___试.Query
{
    partial class FrmCustomerHistoryQuery
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label12 = new System.Windows.Forms.Label();
            this.dgvCustomerHistoryQueryResult = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtKeyWords = new System.Windows.Forms.TextBox();
            this.cmbQueryStype = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnQuery = new System.Windows.Forms.Button();
            this.lblTip = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCustomerHistoryQueryResult)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(13, 227);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(112, 15);
            this.label12.TabIndex = 36;
            this.label12.Text = "查询结果信息：";
            // 
            // dgvCustomerHistoryQueryResult
            // 
            this.dgvCustomerHistoryQueryResult.AllowUserToAddRows = false;
            this.dgvCustomerHistoryQueryResult.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvCustomerHistoryQueryResult.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCustomerHistoryQueryResult.Location = new System.Drawing.Point(13, 254);
            this.dgvCustomerHistoryQueryResult.Margin = new System.Windows.Forms.Padding(4);
            this.dgvCustomerHistoryQueryResult.MultiSelect = false;
            this.dgvCustomerHistoryQueryResult.Name = "dgvCustomerHistoryQueryResult";
            this.dgvCustomerHistoryQueryResult.ReadOnly = true;
            this.dgvCustomerHistoryQueryResult.RowHeadersWidth = 51;
            this.dgvCustomerHistoryQueryResult.RowTemplate.Height = 23;
            this.dgvCustomerHistoryQueryResult.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCustomerHistoryQueryResult.Size = new System.Drawing.Size(1166, 285);
            this.dgvCustomerHistoryQueryResult.TabIndex = 34;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtKeyWords);
            this.groupBox1.Controls.Add(this.cmbQueryStype);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btnQuery);
            this.groupBox1.Location = new System.Drawing.Point(13, 24);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(1166, 160);
            this.groupBox1.TabIndex = 35;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "查询";
            // 
            // txtKeyWords
            // 
            this.txtKeyWords.Location = new System.Drawing.Point(410, 99);
            this.txtKeyWords.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtKeyWords.Name = "txtKeyWords";
            this.txtKeyWords.Size = new System.Drawing.Size(192, 25);
            this.txtKeyWords.TabIndex = 7;
            this.txtKeyWords.Enter += new System.EventHandler(this.txtKeyWords_Enter);
            this.txtKeyWords.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtKeyWords_KeyPress);
            // 
            // cmbQueryStype
            // 
            this.cmbQueryStype.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbQueryStype.FormattingEnabled = true;
            this.cmbQueryStype.Items.AddRange(new object[] {
            "身份证号",
            "姓名",
            "房号",
            "查询全部"});
            this.cmbQueryStype.Location = new System.Drawing.Point(410, 38);
            this.cmbQueryStype.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cmbQueryStype.Name = "cmbQueryStype";
            this.cmbQueryStype.Size = new System.Drawing.Size(192, 23);
            this.cmbQueryStype.TabIndex = 6;
            this.cmbQueryStype.SelectionChangeCommitted += new System.EventHandler(this.cmbQueryStype_SelectionChangeCommitted);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(334, 106);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 15);
            this.label2.TabIndex = 5;
            this.label2.Text = "关 键 字";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(334, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 15);
            this.label1.TabIndex = 5;
            this.label1.Text = "查询方式";
            // 
            // btnQuery
            // 
            this.btnQuery.Location = new System.Drawing.Point(674, 63);
            this.btnQuery.Margin = new System.Windows.Forms.Padding(4);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(100, 29);
            this.btnQuery.TabIndex = 4;
            this.btnQuery.Text = "查询";
            this.btnQuery.UseVisualStyleBackColor = true;
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // lblTip
            // 
            this.lblTip.AutoSize = true;
            this.lblTip.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblTip.ForeColor = System.Drawing.Color.Red;
            this.lblTip.Location = new System.Drawing.Point(12, 188);
            this.lblTip.Name = "lblTip";
            this.lblTip.Size = new System.Drawing.Size(74, 19);
            this.lblTip.TabIndex = 37;
            this.lblTip.Text = "查询完毕!";
            this.lblTip.Visible = false;
            // 
            // FrmCustomerHistoryQuery
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1192, 552);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.dgvCustomerHistoryQueryResult);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lblTip);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmCustomerHistoryQuery";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "顾客历史查询";
            this.Load += new System.EventHandler(this.FrmCustomerHistoryQuery_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCustomerHistoryQueryResult)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.DataGridView dgvCustomerHistoryQueryResult;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtKeyWords;
        private System.Windows.Forms.ComboBox cmbQueryStype;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnQuery;
        private System.Windows.Forms.Label lblTip;
    }
}