﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DAL;

namespace 酒店管理系统___试
{
    public partial class FrmConfig : Form
    {
        private DAL.FloorManager floor = new FloorManager();
        private DAL.RoomManager room = new RoomManager();
        List<Models.Floor> floorList;
        public FrmConfig()
        {
            InitializeComponent();
        }

        private void FrmConfig_Load(object sender, EventArgs e)
        {
            
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            //txtFloorNum.Enabled = btnOK.Enabled = false;    //禁用
            if (! floor.IsFloorTableExits())
            {
                floor.CreatTable();
            }
            groupBox1.Controls.Clear();
            int lblX = 150;
            int lblY = 45;
            int textBoxX = 220;
            int textBoxY = 42;
            if (txtFloorNum.Text.Trim() != "")
            {
                for (int i = 1; i <= Convert.ToInt32(txtFloorNum.Text.Trim()); i++)
                {
                    Label lbl = new Label();
                    lbl.Text = i.ToString()+"楼房间数";
                    lbl.AutoSize = true;
                    lbl.Location = new Point(lblX, lblY);
                    groupBox1.Controls.Add(lbl);

                    TextBox textBox = new TextBox();
                    textBox.Name = "楼层"+i;
                    textBox.Tag = i;
                    textBox.Size = new Size(180,25);
                    textBox.Location = new Point(textBoxX, textBoxY);
                    groupBox1.Controls.Add(textBox);

                    lblY += 50;
                    textBoxY = lblY-3;
                }
            }
        }



        private void btnSave_Click(object sender, EventArgs e)
        {
            foreach (Control c in groupBox1.Controls)
            {
                if (c is TextBox && c.Text.Trim() != "")
                {
                    try
                    {
                        floor.InsertInfoIntoFloorTable(Convert.ToInt32(c.Tag), Convert.ToInt32(c.Text.Trim()));
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    
                }
            }
            System.Threading.Thread.Sleep(300);
            Insert();

            MessageBox.Show("楼层房间配置成功！");
            this.Close();
        }

        /// <summary>
        /// 初始化房间信息（类型、状态、、、）
        /// </summary>
        public void Insert()
        {
            floorList = floor.GetFloorInfo();
            Console.WriteLine(floorList[0]);

            for (int i = 1; i <= floorList.Count; i++)
            {
                for (int j = 1; j <= floorList[i - 1].RoomNum; j++)
                {
                    Models.RoomInfo roomInfo = new Models.RoomInfo
                    {
                        FloorNum = i,
                        RoomNum = i.ToString() + "-" + j,
                        RoomTypeID = 1,
                        RoomStateID = 0,
                        Remarks = "",
                    };

                    room.InsertRoomInfo(roomInfo);
                }
            }
        }

    }
}
