﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Models;

namespace 酒店管理系统___试
{
    public partial class FrmGetARoom : Form
    {
        private DAL.CustomerManager customerManager = new DAL.CustomerManager();
        private DAL.RoomManager roomManager = new DAL.RoomManager();

        public FrmGetARoom(UCRoomInfo.RoomInfo room)
        {
            InitializeComponent();
            txtRoomNum.Text = room.RoomNumber;
            txtRoomType.Text = room.RoomType;
            txtUnitPrice.Text = room.RoomUnitPrice;
        }
        //窗口加载
        private void FrmGetARoom_Load(object sender, EventArgs e)
        {
            switch (txtRoomType.Text)
            {
                case "单标":
                    groupBox2.Enabled = false;
                    groupBox3.Enabled = false;
                    break;
                case "双标":
                    groupBox2.Enabled = true;
                    groupBox3.Enabled = false;
                    break;
                case "三人":
                    groupBox2.Enabled = true;
                    groupBox3.Enabled = true;
                    break;
                case "大床":
                    groupBox2.Enabled = false;
                    groupBox3.Enabled = false;
                    break;
                case "套间":
                    groupBox2.Enabled = false;
                    groupBox3.Enabled = false;
                    break;
                case "商务":
                    groupBox2.Enabled = false;
                    groupBox3.Enabled = false;
                    break;
                case "豪华":
                    groupBox2.Enabled = false;
                    groupBox3.Enabled = false;
                    break;
            }
        }
        //确定开房
        private void btnOK_Click(object sender, EventArgs e)
        {
            if (txtIDCrad1.Text.Trim().Length != 0 && txtName1.Text.Trim().Length != 0 && cmbSex1.SelectedIndex != -1 && txtBookedDay.Text.Trim().Length != 0 && txtDeposit.Text.Trim().Length != 0)
            {
                if (groupBox2.Enabled==false && groupBox3.Enabled == false) //单人身份
                {
                    //封装customer对象
                    Customer customer = new Customer
                    {
                        IDCardNum=txtIDCrad1.Text.Trim(),
                        Name=txtName1.Text.Trim(),
                        Sex=cmbSex1.Text,
                        RoomNum=txtRoomNum.Text,
                        BookedTime=DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),  //入住时间
                        BookedDay = Convert.ToInt32(txtBookedDay.Text.Trim()),  //预住时间
                        Deposit=Convert.ToDouble(txtDeposit.Text.Trim()),
                        Remarks=txtRemarks.Text.Trim(),
                    };
                    //封装customerHistory对象
                    CustomerHistory customerHistory = new CustomerHistory
                    {
                        IDCardNum = txtIDCrad1.Text.Trim(),
                        Name = txtName1.Text.Trim(),
                        Sex = cmbSex1.Text,
                        RoomNum = txtRoomNum.Text,
                        StartTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),  //开始时间
                        EndTime = "---",  //结束时间
                        Remarks = "",
                    };
                    //插入顾客信息记录、顾客历史记录
                    int rs1 = customerManager.AddRecord(customer); //插入顾客信息记录
                    int rs2 = customerManager.AddHistoryRecord(customerHistory); //插入顾客历史记录
                    int rs3 = roomManager.UpdateRoomState(customer.RoomNum,1);  //更新房间状态（有客）
                    if (rs1 > 0 && rs2 > 0 && rs3 > 0)
                    {
                        MessageBox.Show("开房成功！","提示");
                        
                        //清空textbox
                        ClearTextBoxs();
                    }
                    else
                    {
                        MessageBox.Show("失败！请检查！", "提示");
                    }
                }
                if (groupBox2.Enabled == true && groupBox3.Enabled == false)  //双人身份
                {
                    //封装customer对象1
                    Customer customer1 = new Customer
                    {
                        IDCardNum = txtIDCrad1.Text.Trim(),
                        Name = txtName1.Text.Trim(),
                        Sex = cmbSex1.Text,
                        RoomNum = txtRoomNum.Text,
                        BookedTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),  //入住时间
                        BookedDay = Convert.ToInt32(txtBookedDay.Text.Trim()),  //预住时间
                        Deposit = Convert.ToDouble(txtDeposit.Text.Trim()),
                        Remarks = txtRemarks.Text.Trim(),
                    };
                    //封装customerHistory对象1
                    CustomerHistory customerHistory1 = new CustomerHistory
                    {
                        IDCardNum = txtIDCrad1.Text.Trim(),
                        Name = txtName1.Text.Trim(),
                        Sex = cmbSex1.Text,
                        RoomNum = txtRoomNum.Text,
                        StartTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),  //开始时间
                        EndTime = "---",  //结束时间
                        Remarks = "",
                    };
                    //封装customer对象2
                    Customer customer2 = new Customer
                    {
                        IDCardNum = txtIDCrad2.Text.Trim(),
                        Name = txtName2.Text.Trim(),
                        Sex = cmbSex2.Text,
                        RoomNum = txtRoomNum.Text,
                        BookedTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),  //入住时间
                        BookedDay = Convert.ToInt32(txtBookedDay.Text.Trim()),  //预住时间
                        Deposit = 0,  //押金，双人只计一份押金
                        Remarks = "双人间只计一份押金",
                    };
                    //封装customerHistory对象2
                    CustomerHistory customerHistory2 = new CustomerHistory
                    {
                        IDCardNum = txtIDCrad2.Text.Trim(),
                        Name = txtName2.Text.Trim(),
                        Sex = cmbSex2.Text,
                        RoomNum = txtRoomNum.Text,
                        StartTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),  //开始时间
                        EndTime = "---",  //结束时间
                        Remarks = "",
                    };
                    //插入顾客1、2信息记录，顾客历史1、2
                    int rs1 = customerManager.AddRecord(customer1); //插入顾客1
                    int rs2 = customerManager.AddRecord(customer2); //插入顾客2
                    int rs3 = customerManager.AddHistoryRecord(customerHistory1); //插入顾客1历史
                    int rs4 = customerManager.AddHistoryRecord(customerHistory2); //插入顾客2历史
                    int rs5 = roomManager.UpdateRoomState(customer1.RoomNum,1);
                    if (rs1 > 0 && rs2> 0 && rs3> 0 && rs4> 0 && rs5 > 0)
                    {
                        MessageBox.Show("开房成功！", "提示");
                        //清空textbox
                        ClearTextBoxs();
                    }
                    else
                    {
                        MessageBox.Show("失败！请检查！", "提示");
                    }
                }
                if (groupBox2.Enabled == true && groupBox3.Enabled == true)  //三人身份
                {
                    //封装customer对象1
                    Customer customer1 = new Customer
                    {
                        IDCardNum = txtIDCrad1.Text.Trim(),
                        Name = txtName1.Text.Trim(),
                        Sex = cmbSex1.Text,
                        RoomNum = txtRoomNum.Text,
                        BookedTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),  //入住时间
                        BookedDay = Convert.ToInt32(txtBookedDay.Text.Trim()),  //预住时间
                        Deposit = Convert.ToDouble(txtDeposit.Text.Trim()),
                        Remarks = txtRemarks.Text.Trim(),
                    };
                    //封装customer对象2
                    Customer customer2 = new Customer
                    {
                        IDCardNum = txtIDCrad2.Text.Trim(),
                        Name = txtName2.Text.Trim(),
                        Sex = cmbSex2.Text,
                        RoomNum = txtRoomNum.Text,
                        BookedTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),  //入住时间
                        BookedDay = Convert.ToInt32(txtBookedDay.Text.Trim()),  //预住时间
                        Deposit = 0,  //押金，双人只计一份押金
                        Remarks = "三人间只计一份押金",
                    };
                    //封装customer对象3
                    Customer customer3 = new Customer
                    {
                        IDCardNum = txtIDCrad3.Text.Trim(),
                        Name = txtName3.Text.Trim(),
                        Sex = cmbSex3.Text,
                        RoomNum = txtRoomNum.Text,
                        BookedTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),  //入住时间
                        BookedDay = Convert.ToInt32(txtBookedDay.Text.Trim()),  //预住时间
                        Deposit = 0,  //押金，双人只计一份押金
                        Remarks = "三人间只计一份押金",
                    };
                    //封装customerHistory对象1
                    CustomerHistory customerHistory1 = new CustomerHistory
                    {
                        IDCardNum = txtIDCrad1.Text.Trim(),
                        Name = txtName1.Text.Trim(),
                        Sex = cmbSex1.Text,
                        RoomNum = txtRoomNum.Text,
                        StartTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),  //开始时间
                        EndTime = "---",  //结束时间
                        Remarks = "",
                    };
                    //封装customerHistory对象2
                    CustomerHistory customerHistory2 = new CustomerHistory
                    {
                        IDCardNum = txtIDCrad2.Text.Trim(),
                        Name = txtName2.Text.Trim(),
                        Sex = cmbSex2.Text,
                        RoomNum = txtRoomNum.Text,
                        StartTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),  //开始时间
                        EndTime = "---",  //结束时间
                        Remarks = "",
                    };
                    //封装customerHistory对象3
                    CustomerHistory customerHistory3 = new CustomerHistory
                    {
                        IDCardNum = txtIDCrad3.Text.Trim(),
                        Name = txtName3.Text.Trim(),
                        Sex = cmbSex3.Text,
                        RoomNum = txtRoomNum.Text,
                        StartTime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),  //开始时间
                        EndTime = "---",  //结束时间
                        Remarks = "",
                    };
                    //插入顾客1、2、3信息记录，顾客历史1、2、3
                    int rs1 = customerManager.AddRecord(customer1); //插入顾客1
                    int rs2 = customerManager.AddRecord(customer2); //插入顾客2
                    int rs3 = customerManager.AddRecord(customer2); //插入顾客2
                    int rs4 = customerManager.AddHistoryRecord(customerHistory1); //插入顾客1历史
                    int rs5 = customerManager.AddHistoryRecord(customerHistory2); //插入顾客2历史
                    int rs6 = customerManager.AddHistoryRecord(customerHistory2); //插入顾客3历史
                    int rs7 = roomManager.UpdateRoomState(customer1.RoomNum, 1);
                    if (rs1 > 0 && rs2 > 0 && rs3 > 0 && rs4 > 0 && rs5 > 0 && rs6 > 0 && rs7 > 0)
                    {
                        MessageBox.Show("开房成功！", "提示");
                        //清空textbox
                        ClearTextBoxs();
                    }
                    else
                    {
                        MessageBox.Show("失败！请检查！", "提示");
                    }
                }
            }
            else
            {
                MessageBox.Show("请填写完整！","提示");
            }
        }
        //取消开房
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //关闭
        private void FrmGetARoom_FormClosed(object sender, FormClosedEventArgs e)
        {
            DialogResult = DialogResult.Yes; 
        }
        /// <summary>
        /// 清空编辑框
        /// </summary>
        public void ClearTextBoxs()
        {
            foreach (Control c in this.Controls)
            {
                if (c is TextBox)
                {
                    c.Text = "";
                }
            }
            foreach (Control c in groupBox1.Controls)
            {
                if (c is TextBox)
                {
                    c.Text = "";
                }
            }
            foreach (Control c in groupBox2.Controls)
            {
                if (c is TextBox)
                {
                    c.Text = "";
                }
            }
            foreach (Control c in groupBox3.Controls)
            {
                if (c is TextBox)
                {
                    c.Text = "";
                }
            }
        }
    }
}
