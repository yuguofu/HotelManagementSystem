﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Models;

namespace 酒店管理系统___试
{
    public partial class FrmReserve : Form
    {
        private DAL.RoomManager roomManager = new DAL.RoomManager();
        private DAL.CustomerManager customerManager = new DAL.CustomerManager();
        DAL.ReserveMamager reserveMamager = new DAL.ReserveMamager();

        public FrmReserve(UCRoomInfo.RoomInfo room)
        {
            InitializeComponent();


            List<Models.RoomInfo> roomInfoList = roomManager.GetRoomInfo(room.RoomNumber);
            List<Customer> customerList = customerManager.GetCustomerInfo(room.RoomNumber);
            txtFloor.Text = room.RoomNumber.Split('-')[0] + "楼";
            txtRoomNum.Text = room.RoomNumber;
            txtRoomType.Text = roomInfoList[0].RoomTypeName;
            txtUnitPrice.Text = roomInfoList[0].UnitPrice.ToString() + "/天";
            txtRoomState.Text = room.RoomState;

            txtReserveRoomNum.Text = room.RoomNumber;
            txtReserveTime.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm");
        }

        private void FrmReserve_Load(object sender, EventArgs e)
        {

        }

        //确认预定
        private void btnOK_Click(object sender, EventArgs e)
        {
            if (txtName.Text.Trim().Length !=0 && txtReserveTime.Text.Trim().Length !=0)
            {
                Reserve reserve = new Reserve { 
                    IDCradNum=txtIDCard.Text.Trim(),
                    Name=txtName.Text.Trim(),
                    Sex=cmbSex.Text,
                    ReserveRoomNum=txtReserveRoomNum.Text,
                    ReserveTime=txtReserveTime.Text.Trim(),
                    TimeSlice=dateTimePicker1.Value.ToString()+"-"+dateTimePicker2.Value.ToString(),
                };
                int rs1 = reserveMamager.ReserveRoom(reserve);
                int rs2 = roomManager.UpdateRoomState(txtReserveRoomNum.Text,3);  //更新房间状态
                if (rs1 > 0 && rs2 > 0)
                {
                    MessageBox.Show("预定成功！");
                }
            }
            else
            {
                MessageBox.Show("姓名、预定房号、预定时间、预定时段 为必填！");
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmReserve_FormClosed(object sender, FormClosedEventArgs e)
        {
            DialogResult = DialogResult.Yes;
        }
    }
}
