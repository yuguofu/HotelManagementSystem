﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using DAL;
using Models;
using UCRoomInfo;

namespace 酒店管理系统___试
{
    public partial class FrmMain : Form
    {
        int x = 9;  //房间控件定位x
        int y = 30;  //房间控件定位y
        private FloorManager floorManager = new DAL.FloorManager();
        private RoomManager roomManager = new RoomManager();
        private CustomerManager customerManager = new CustomerManager();
        private ReserveMamager reserveMamager = new ReserveMamager();
        List<Models.Floor> floorList;
        List<Models.RoomInfo> roomInfoList;


        /// <summary>
        /// 枚举 房间状态
        /// </summary>
        public enum RoomState : int
        {
            空闲 = 0,
            有客 = 1,
            打扫 = 2,
            预定 = 3,
            禁用 = 4,
        }

        public FrmMain()
        {
            InitializeComponent();
            if (Program.currentUser.Power !=0)
            {
                单价管理ToolStripMenuItem.Enabled = false;
                vIP管理ToolStripMenuItem.Enabled = false;
                用户管理ToolStripMenuItem.Enabled = false;

                
            }
        }
        //窗口加载
        private void FrmMain_Load(object sender, EventArgs e)
        {
            toolStripStatusCurrentUser.Text += Program.currentUser.UserName + " （" + Program.currentUser.Remarks + "） | ";
            toolStripStatusLoginTime.Text += DateTime.Now.ToString() + " | ";
            timer1.Start();

            floorList = floorManager.GetFloorInfo();   //楼层信息
            if (floorManager.IsFloorTableExits())  //楼层表存在
            {
                foreach (var item in floorList)
                {
                    roomInfoList = roomManager.GetRoomInfo(item.FloorNum);
                    CreatRoom(item.RoomNum);
                    x = 9;
                    y += 150;

                    //创建 红色分割线
                    Label lbl = new Label();
                    lbl.AutoSize = false;
                    lbl.FlatStyle = FlatStyle.Flat;
                    lbl.BackColor = Color.Red;
                    lbl.Size = new Size(1132,2);
                    lbl.Location = new Point(x,y-15);
                    this.Controls.Add(lbl);
                }
                
            }
            else    //楼层表不存在，进行配置
            {
                FrmConfig frmConfig = new FrmConfig();
                frmConfig.ShowDialog();
                FrmMain_Load(sender,e);
            }
        }

        /// <summary>
        /// 创建房间控件
        /// </summary>
        /// <param name="roomNum"></param>
        public void CreatRoom(int roomNum)
        {
            for (int i = 1; i <= roomNum; i++)
            {
                UCRoomInfo.RoomInfo roomInfo = new UCRoomInfo.RoomInfo();
                roomInfo.RoomNumber = roomInfoList[i - 1].RoomNum;
                roomInfo.RoomTypeID = roomInfoList[i - 1].RoomTypeID.ToString();
                roomInfo.RoomType = roomInfoList[i - 1].RoomTypeName;
                roomInfo.RoomUnitPrice = roomInfoList[i - 1].UnitPrice.ToString();
                roomInfo.RoomStateID = roomInfoList[i - 1].RoomStateID.ToString();
                roomInfo.RoomState = roomInfoList[i - 1].RoomStateName;
                switch (roomInfo.RoomStateID)   //根据房间状态改变颜色
                {
                    case "1":   //有客
                        roomInfo.BackColor = Color.Thistle; //淡紫色
                        break;
                    case "2":   //打扫
                        roomInfo.BackColor = Color.Khaki;   //淡黄色
                        break;
                    case "3":   //锁定
                        roomInfo.BackColor = Color.MistyRose;   //淡红色
                        break;
                    case "4":   //禁用
                        roomInfo.BackColor = Color.DarkGray;    //淡灰色
                        break;
                }
                roomInfo.Location = new Point(x, y);
                roomInfo.DoubleClick += new EventHandler(RoomInfo_DoubleClick);   //双击事件委托
                roomInfo.MouseClick += new MouseEventHandler(RoomInfo_MouseClick);  //鼠标点击事件委托
                roomInfo.ContextMenuStrip = cmsRoomRightBtn;
                this.Controls.Add(roomInfo);
                x += 115;
                if (i % 10 == 0)
                {
                    x = 9;
                    y += 150;
                }
            }
        }

 
        /// <summary>
        /// UCRoomInfo控件鼠标点击事件   弹出右键菜单
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RoomInfo_MouseClick(object sender, MouseEventArgs e)
        {
            UCRoomInfo.RoomInfo room = (UCRoomInfo.RoomInfo)sender;
            if (e.Button==MouseButtons.Right)
            {
                cmsRoomRightBtn.Show(e.X, e.Y);
            }
        }

        /// <summary>
        /// UCRoomInfo控件双击事件  弹出详情面板
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RoomInfo_DoubleClick(object sender, EventArgs e)
        {
            UCRoomInfo.RoomInfo roomInfo = (UCRoomInfo.RoomInfo)sender;
            if (roomInfo.RoomState !="预定")
            {
                List<Models.RoomInfo> roomInfoList = roomManager.GetRoomInfo(roomInfo.RoomNumber);
                List<Customer> customerList = customerManager.GetCustomerInfo(roomInfo.RoomNumber);
                pnlDetails.Visible = true;
                lblFloor.Text = roomInfo.RoomNumber.Split('-')[0] + "楼";
                lblRoomNum.Text = roomInfo.RoomNumber;
                lblRoomType.Text = roomInfoList[0].RoomTypeName;
                lblUnitPrice.Text = roomInfoList[0].UnitPrice.ToString() + "/天";
                lblRoomState.Text = roomInfo.RoomState;
                //顾客信息
                lblCustomerInfo.Text = "";
                if (customerList.Count > 0)
                {
                    foreach (var item in customerList)
                    {
                        lblCustomerInfo.Text += "身份证号:" + item.IDCardNum + " | 姓名:" + item.Name + " | 性别:" + item.Sex + " | 房号:" + item.RoomNum + " | 入住时间:" + item.BookedTime + " | 押金:" + item.Deposit + "\n";
                    }
                }
                else
                {
                    lblCustomerInfo.Text = "无";
                }
                lblRemarks.Text = roomInfoList[0].Remarks;
            }
            if (roomInfo.RoomState == "预定")
            {
                List<Models.RoomInfo> roomInfoList = roomManager.GetRoomInfo(roomInfo.RoomNumber);
                List<Models.Reserve> reservesList = reserveMamager.GetReservesInfo(roomInfo.RoomNumber);
                pnlReserveDetails.Visible = true;
                lblFloor2.Text = roomInfo.RoomNumber.Split('-')[0] + "楼";
                lblRoomNum2.Text = roomInfo.RoomNumber;
                lblRoomType2.Text = roomInfoList[0].RoomTypeName;
                lblUnitPrice2.Text = roomInfoList[0].UnitPrice.ToString() + "/天";
                lblRoomState2.Text = roomInfo.RoomState;
                //预定信息
                //Console.WriteLine(reservesList.Count);
                lblReserveInfo.Text = "";
                if (reservesList.Count > 0)
                {
                    foreach (var item in reservesList)
                    {
                        lblReserveInfo.Text += "身份证号:" + item.IDCradNum + " | 姓名:" + item.Name + " | 性别:" + item.Sex + " | 房号:" + item.ReserveRoomNum + " | 预定时间:" + item.ReserveTime + " | 预定时段:" + item.TimeSlice;
                    }
                }
                
            }
        }


        //关闭详情面板
        private void btnClose_Click(object sender, EventArgs e)
        {
            pnlDetails.Visible = false;
        }

        private void btnClose2_Click(object sender, EventArgs e)
        {
            pnlReserveDetails.Visible = false;
        }

        //菜单弹出前根据房间状态禁用某些菜单
        private void CmsRoomRightBtn_Opening(object sender, CancelEventArgs e)
        {
            UCRoomInfo.RoomInfo room = (UCRoomInfo.RoomInfo)cmsRoomRightBtn.SourceControl;
            //根据房间状态禁用某些菜单
            switch (room.RoomStateID)     //根据房间状态显示菜单
            {
                case "0":  //空闲  只能开房、打扫、锁定、禁用
                    开房ToolStripMenuItem.Enabled = true;
                    退房ToolStripMenuItem.Enabled = false;
                    打扫ToolStripMenuItem.Enabled = true;
                    结束打扫ToolStripMenuItem.Enabled = false;
                    锁定ToolStripMenuItem.Enabled = true;
                    结束锁定ToolStripMenuItem.Enabled = false;
                    禁用ToolStripMenuItem.Enabled = true;
                    结束禁用ToolStripMenuItem.Enabled = false;
                    break;
                case "1":  //有客  只能退房
                    开房ToolStripMenuItem.Enabled = false;
                    退房ToolStripMenuItem.Enabled = true;
                    打扫ToolStripMenuItem.Enabled = false;
                    结束打扫ToolStripMenuItem.Enabled = false;
                    锁定ToolStripMenuItem.Enabled = false;
                    结束锁定ToolStripMenuItem.Enabled = false;
                    禁用ToolStripMenuItem.Enabled = false;
                    结束禁用ToolStripMenuItem.Enabled = false;
                    break;
                case "2":  //打扫  只能结束打扫
                    开房ToolStripMenuItem.Enabled = false;
                    退房ToolStripMenuItem.Enabled = false;
                    打扫ToolStripMenuItem.Enabled = false;
                    结束打扫ToolStripMenuItem.Enabled = true;
                    锁定ToolStripMenuItem.Enabled = false;
                    结束锁定ToolStripMenuItem.Enabled = false;
                    禁用ToolStripMenuItem.Enabled = false;
                    结束禁用ToolStripMenuItem.Enabled = false;
                    break;
                case "3":  //锁定  只能结束锁定
                    开房ToolStripMenuItem.Enabled = false;
                    退房ToolStripMenuItem.Enabled = false;
                    打扫ToolStripMenuItem.Enabled = false;
                    结束打扫ToolStripMenuItem.Enabled = false;
                    锁定ToolStripMenuItem.Enabled = false;
                    结束锁定ToolStripMenuItem.Enabled = true;
                    禁用ToolStripMenuItem.Enabled = false;
                    结束禁用ToolStripMenuItem.Enabled = false;
                    break;
                case "4":  //禁用  只能结束禁用
                    开房ToolStripMenuItem.Enabled = false;
                    退房ToolStripMenuItem.Enabled = false;
                    打扫ToolStripMenuItem.Enabled = false;
                    结束打扫ToolStripMenuItem.Enabled = false;
                    锁定ToolStripMenuItem.Enabled = false;
                    结束锁定ToolStripMenuItem.Enabled = false;
                    禁用ToolStripMenuItem.Enabled = false;
                    结束禁用ToolStripMenuItem.Enabled = true;
                    break;
            }
        }
        //开房
        private void 开房ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UCRoomInfo.RoomInfo room = (UCRoomInfo.RoomInfo)cmsRoomRightBtn.SourceControl;
            FrmGetARoom frmGetARoom = new FrmGetARoom(room);
            DialogResult result = frmGetARoom.ShowDialog();
            if (result==DialogResult.Yes)
            {
                this.Controls.Clear();
                this.InitializeComponent();
                x = 9;
                y = 30;
                this.FrmMain_Load(sender, e);
            }
        }
        //退房
        private void 退房ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UCRoomInfo.RoomInfo room = (UCRoomInfo.RoomInfo)cmsRoomRightBtn.SourceControl;
            FrmCheckedOut frmCheckedOut = new FrmCheckedOut(room);
            DialogResult result = frmCheckedOut.ShowDialog();
            if (result == DialogResult.Yes)
            {
                this.Controls.Clear();
                this.InitializeComponent();
                x = 9;
                y = 30;
                this.FrmMain_Load(sender, e);
            }
        }
        //打扫
        private void 打扫ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UCRoomInfo.RoomInfo room = (UCRoomInfo.RoomInfo)cmsRoomRightBtn.SourceControl;
            if (roomManager.UpdateRoomState(room.RoomNumber, (int)RoomState.打扫) >0)
            {
                MessageBox.Show("设置成功！");
                刷新ToolStripMenuItem_Click(sender, e);
            }
        }
        //结束打扫
        private void 结束打扫ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UCRoomInfo.RoomInfo room = (UCRoomInfo.RoomInfo)cmsRoomRightBtn.SourceControl;
            if (roomManager.UpdateRoomState(room.RoomNumber, (int)RoomState.空闲) > 0)
            {
                刷新ToolStripMenuItem_Click(sender, e);
            }
        }
        //预定
        private void 预定ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UCRoomInfo.RoomInfo room = (UCRoomInfo.RoomInfo)cmsRoomRightBtn.SourceControl;
            FrmReserve frmReserve = new FrmReserve(room);
            DialogResult result = frmReserve.ShowDialog();
            if (result==DialogResult.Yes)
            {
                刷新ToolStripMenuItem_Click(sender,e);
            }
        }
        //结束锁定
        private void 结束锁定ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UCRoomInfo.RoomInfo room = (UCRoomInfo.RoomInfo)cmsRoomRightBtn.SourceControl;
            int rs1 = roomManager.UpdateRoomState(room.RoomNumber, (int)RoomState.空闲);  //更新房间状态（空闲）
            int rs2 = reserveMamager.DeleReserve(room.RoomNumber);  //删除预定记录
            if (rs1 > 0 && rs2 > 0)
            {
                刷新ToolStripMenuItem_Click(sender, e);
            }
        }
        //禁用
        private void 禁用ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UCRoomInfo.RoomInfo room = (UCRoomInfo.RoomInfo)cmsRoomRightBtn.SourceControl;
            if (roomManager.UpdateRoomState(room.RoomNumber, (int)RoomState.禁用) > 0)
            {
                MessageBox.Show("设置成功！");
                刷新ToolStripMenuItem_Click(sender, e);
            }
        }
        //结束禁用
        private void 结束禁用ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UCRoomInfo.RoomInfo room = (UCRoomInfo.RoomInfo)cmsRoomRightBtn.SourceControl;
            if (roomManager.UpdateRoomState(room.RoomNumber, (int)RoomState.空闲) > 0)
            {
                刷新ToolStripMenuItem_Click(sender, e);

            }
        }
        //刷新
        private void 刷新ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Controls.Clear();
            this.InitializeComponent();
            x = 9;
            y = 30;
            this.FrmMain_Load(sender, e);
        }
        //退出
        private void 退出ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        //宾客查询
        private void 宾客查询ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Query.FrmCustomerQuery frmCustomerQuery = new Query.FrmCustomerQuery();
            frmCustomerQuery.ShowDialog();
        }
        //VIP查询
        private void vIP查询ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Query.FrmVIPQuery frmVIPQuery = new Query.FrmVIPQuery();
            frmVIPQuery.ShowDialog();
        }
        //顾客历史查询
        private void 顾客历史查询ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Query.FrmCustomerHistoryQuery frmCustomerHistoryQuery = new Query.FrmCustomerHistoryQuery();
            frmCustomerHistoryQuery.ShowDialog();
        }
        //房间管理
        private void 房间管理ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Manager.FrmRoomManager frmRoomManager = new Manager.FrmRoomManager();
            frmRoomManager.ShowDialog();
        }
        //单价管理
        private void 单价管理ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Manager.FrmUnitPriceManager frmUnitPriceManager = new Manager.FrmUnitPriceManager();
            frmUnitPriceManager.ShowDialog();
        }
        //VIP管理
        private void vIP管理ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Manager.FrmVIPManager frmVIPManager = new Manager.FrmVIPManager();
            frmVIPManager.ShowDialog();
        }
        //用户管理
        private void 用户管理ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Manager.FrmUserManager frmUserManager = new Manager.FrmUserManager();
            frmUserManager.ShowDialog();
        }

        private void 关于ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmAbout frmAbout = new FrmAbout();
            frmAbout.ShowDialog();
        }
        //现行时间
        private void timer1_Tick(object sender, EventArgs e)
        {
            toolStripStatusCurrentTime.Text = "现行时间：" + DateTime.Now.ToString() + " | ";
        }
    }
}
