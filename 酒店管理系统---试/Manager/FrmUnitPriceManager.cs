﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Models;

namespace 酒店管理系统___试.Manager
{
    public partial class FrmUnitPriceManager : Form
    {
        private DAL.RoomManager roomManager = new DAL.RoomManager();
        List<RoomType> roomTypesList;
        public FrmUnitPriceManager()
        {
            InitializeComponent();
            roomTypesList = roomManager.GetRoomType();
            //绑定房间类型数据到组合框
            cmbRoomTypeName.DataSource = roomTypesList;
            cmbRoomTypeName.DisplayMember = "TypeName";
            cmbRoomTypeName.ValueMember = "TypeID";
            cmbRoomTypeName.SelectedIndex = -1;
        }

        private void FrmUnitPriceManager_Load(object sender, EventArgs e)
        {
            
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (cmbRoomTypeName.SelectedIndex !=-1 && txtUnitPrice.Text.Trim().Length !=0)
            {
                if (roomManager.UpdateRoomTypeUnitPrice(Convert.ToInt32(cmbRoomTypeName.SelectedValue),Convert.ToDouble(txtUnitPrice.Text.Trim())) >0)
                {
                    MessageBox.Show("设置成功！");
                    txtUnitPrice.Text = "";
                }
            }
            else
            {
                MessageBox.Show("请选择放假了类型并设置单价！");
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cmbRoomTypeName_SelectionChangeCommitted(object sender, EventArgs e)
        {
            txtTypeID.Text = cmbRoomTypeName.SelectedValue.ToString();
            switch (cmbRoomTypeName.SelectedIndex)
            {
                case 0:  //单标
                    txtUnitPrice.Text = roomTypesList[0].UnitPrice.ToString();
                    break;
                case 1:  //单标
                    txtUnitPrice.Text = roomTypesList[1].UnitPrice.ToString();
                    break;
                case 2:  //单标
                    txtUnitPrice.Text = roomTypesList[2].UnitPrice.ToString();
                    break;
                case 3:  //单标
                    txtUnitPrice.Text = roomTypesList[3].UnitPrice.ToString();
                    break;
                case 4:  //单标
                    txtUnitPrice.Text = roomTypesList[4].UnitPrice.ToString();
                    break;
                case 5:  //单标
                    txtUnitPrice.Text = roomTypesList[5].UnitPrice.ToString();
                    break;
                case 6:  //单标
                    txtUnitPrice.Text = roomTypesList[6].UnitPrice.ToString();
                    break;
            }
        }
    }
}
