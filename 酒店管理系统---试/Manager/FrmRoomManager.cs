﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Models;

namespace 酒店管理系统___试.Manager
{
    public partial class FrmRoomManager : Form
    {
        private DAL.RoomManager roomManager = new DAL.RoomManager();

        public FrmRoomManager()
        {
            InitializeComponent();

            //绑定房间类型数据到组合框
            cmbRoomType.DataSource = roomManager.GetRoomType();
            cmbRoomType.DisplayMember = "TypeName";
            cmbRoomType.ValueMember = "TypeID";
            cmbRoomType.SelectedIndex = -1;
            //绑定房间类型数据到组合框
            cmbState.DataSource = roomManager.GetRoomState();
            cmbState.DisplayMember = "StateName"; 
            cmbState.ValueMember = "StateID";
            cmbState.SelectedIndex = -1;
        }

        private void FrmRoomManager_Load(object sender, EventArgs e)
        {
            DataSet ds = roomManager.GetAllRoomInfoDataSet();
            dgvRoomInfo.DataSource = ds.Tables[0];
        }

        private void dgvRoomInfo_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            txtFloor.Text = dgvRoomInfo[0,dgvRoomInfo.CurrentCell.RowIndex].Value.ToString();
            txtRoomNum.Text = dgvRoomInfo[1, dgvRoomInfo.CurrentCell.RowIndex].Value.ToString();
            cmbRoomType.Text = dgvRoomInfo[2, dgvRoomInfo.CurrentCell.RowIndex].Value.ToString();
            txtTypeID.Text = dgvRoomInfo[3, dgvRoomInfo.CurrentCell.RowIndex].Value.ToString();
            cmbState.Text = dgvRoomInfo[4, dgvRoomInfo.CurrentCell.RowIndex].Value.ToString();
            txtStateID.Text = dgvRoomInfo[5, dgvRoomInfo.CurrentCell.RowIndex].Value.ToString();
            txtRemarks.Text = dgvRoomInfo[6, dgvRoomInfo.CurrentCell.RowIndex].Value.ToString();
        }

        private void btnRevise_Click(object sender, EventArgs e)
        {
            if (dgvRoomInfo.CurrentCell.RowIndex != -1)
            {
                if (cmbRoomType.SelectedIndex !=-1 && cmbState.SelectedIndex !=-1)
                {
                    RoomInfo room = new RoomInfo
                    {
                        RoomNum = txtRoomNum.Text,
                        RoomTypeID = Convert.ToInt32(txtTypeID.Text),
                        RoomStateID = Convert.ToInt32(txtStateID.Text),
                        Remarks = txtRemarks.Text.Trim(),
                    };
                    if (cmbState.Text == "有客" || cmbState.Text == "预定")
                    {
                        if (MessageBox.Show("当前房间有客或已预定，确定要修改吗？", "提示", MessageBoxButtons.YesNo) == DialogResult.Yes)
                        {
                            if (roomManager.UpdateRoomInfo(room) > 0)
                            {
                                MessageBox.Show("修改成功！");
                                FrmRoomManager_Load(sender, e);
                            }
                        }
                        else
                        {
                            return;
                        }
                    }
                    else
                    {
                        if (roomManager.UpdateRoomInfo(room) > 0)
                        {
                            MessageBox.Show("修改成功！");
                            FrmRoomManager_Load(sender, e);
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("请选择一条数据！");
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cmbRoomType_SelectionChangeCommitted(object sender, EventArgs e)
        {
            txtTypeID.Text = cmbRoomType.SelectedValue.ToString();
        }

        private void cmbState_SelectionChangeCommitted(object sender, EventArgs e)
        {
            txtStateID.Text = cmbState.SelectedValue.ToString();
        }
    }
}
