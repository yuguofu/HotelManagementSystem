﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Models;
using DAL;

namespace 酒店管理系统___试.Manager
{
    public partial class FrmVIPManager : Form
    {
        private VIPManager vIPManager = new VIPManager();
        public FrmVIPManager()
        {
            InitializeComponent();
            
        }

        private void FrmVIPManager_Load(object sender, EventArgs e)
        {
            //绑定数据到dgv
            DataSet ds = vIPManager.GetAllVIPInfoDataSet();
            dgvVIPInfo.DataSource = ds.Tables[0];
            //获取VIP折率，并在lable显示提示默认折率
            string discount = vIPManager.GetVIPDiscount().ToString();
            if (discount=="8")
            {
                lblVIPDiscountTip.Text = "默认VIP折率为 8 折，请及时修改。";
                lblVIPDiscountTip.Visible = true;
            }

        }



        //单击列表
        private void dgvVIPInfo_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            txtIDCard.Text = dgvVIPInfo[1,dgvVIPInfo.CurrentCell.RowIndex].Value.ToString();
            txtName.Text = dgvVIPInfo[2, dgvVIPInfo.CurrentCell.RowIndex].Value.ToString();
            cmbSex.Text = dgvVIPInfo[3, dgvVIPInfo.CurrentCell.RowIndex].Value.ToString();
            txtAge.Text = dgvVIPInfo[4, dgvVIPInfo.CurrentCell.RowIndex].Value.ToString();
        }
        //添加
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (txtIDCard.Text.Trim().Length !=0 && txtName.Text.Trim().Length !=0 && cmbSex.SelectedIndex !=-1 && txtAge.Text.Trim().Length !=0)
            {
                if (vIPManager.IsExistByIDCard(txtIDCard.Text.Trim()))  //该身份证号已是VIP
                {
                    MessageBox.Show("该身份证号已是VIP！");
                    return;
                }
                else
                {
                    VIP vIP = new VIP
                    {
                        IDCardNum = txtIDCard.Text.Trim(),
                        Name = txtName.Text.Trim(),
                        Sex = cmbSex.Text,
                        Age = Convert.ToInt32(txtAge.Text.Trim()),
                    };
                    if (vIPManager.AddVIP(vIP) > 0)
                    {
                        MessageBox.Show("添加成功！");
                        FrmVIPManager_Load(sender, e);
                    }
                }
            }
            else
            {
                MessageBox.Show("请填写完整！");
            }
        }


        //修改
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (txtIDCard.Text.Trim().Length != 0 && txtName.Text.Trim().Length != 0 && cmbSex.SelectedIndex != -1 && txtAge.Text.Trim().Length != 0)
            {
                if (vIPManager.IsExistByIDCard(txtIDCard.Text.Trim()))  //身份证号已存在
                {
                    MessageBox.Show("该身份证号已存在！");
                    return;
                }
                else
                {
                    VIP vIP = new VIP
                    {
                        ID = Convert.ToInt32(dgvVIPInfo[0, dgvVIPInfo.CurrentCell.RowIndex].Value),
                        IDCardNum = txtIDCard.Text.Trim(),
                        Name = txtName.Text.Trim(),
                        Sex = cmbSex.Text,
                        Age = Convert.ToInt32(txtAge.Text.Trim()),
                    };
                    if (vIPManager.UpdateVIP(vIP) > 0)
                    {
                        MessageBox.Show("修改成功！");
                        FrmVIPManager_Load(sender, e);
                    }
                }
            }
            else
            {
                MessageBox.Show("请填写完整！");
            }
        }


        //删除
        private void btnDele_Click(object sender, EventArgs e)
        {
            if (dgvVIPInfo.CurrentCell.RowIndex != -1)
            {
                if (vIPManager.DeleVIP(Convert.ToInt32(dgvVIPInfo[0, dgvVIPInfo.CurrentCell.RowIndex].Value)) > 0)
                {
                    MessageBox.Show("删除成功！");
                    FrmVIPManager_Load(sender, e);
                }
            }
            else
            {
                MessageBox.Show("请选择一条记录！");
            }
        }


        //退出
        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //设置VIP折率
        private void btnDiscountSet_Click(object sender, EventArgs e)
        {
            if (txtDiscount.Text.Trim().Length !=0)
            {
                if (vIPManager.SetDiscount(Convert.ToDouble(txtDiscount.Text.Trim())) >0)
                {
                    MessageBox.Show("VIP折率设置成功！");
                }
                else
                {
                    MessageBox.Show("VIP折率设置失败！");
                }
            }
            else
            {
                MessageBox.Show("请输入要设置的VIP折率！");
            }
        }

       
    }
}
