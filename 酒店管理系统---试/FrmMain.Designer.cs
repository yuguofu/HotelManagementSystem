﻿namespace 酒店管理系统___试
{
    partial class FrmMain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.mnsQuery = new System.Windows.Forms.MenuStrip();
            this.查询ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.宾客查询ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vIP查询ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.顾客历史查询ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.房间管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.单价管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vIP管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.用户管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.帮助ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.刷新ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.退出ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.关于ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cmsRoomRightBtn = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.开房ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.退房ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.打扫ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.结束打扫ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.锁定ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.结束锁定ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.禁用ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.结束禁用ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pnlDetails = new System.Windows.Forms.Panel();
            this.lblRemarks = new System.Windows.Forms.Label();
            this.lblCustomerInfo = new System.Windows.Forms.Label();
            this.lblRoomState = new System.Windows.Forms.Label();
            this.lblUnitPrice = new System.Windows.Forms.Label();
            this.lblRoomType = new System.Windows.Forms.Label();
            this.lblFloor = new System.Windows.Forms.Label();
            this.lblRoomNum = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlReserveDetails = new System.Windows.Forms.Panel();
            this.lblReserveInfo = new System.Windows.Forms.Label();
            this.lblRoomState2 = new System.Windows.Forms.Label();
            this.lblUnitPrice2 = new System.Windows.Forms.Label();
            this.lblRoomType2 = new System.Windows.Forms.Label();
            this.lblFloor2 = new System.Windows.Forms.Label();
            this.lblRoomNum2 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.btnClose2 = new System.Windows.Forms.Button();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.ssr = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusCurrentUser = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLoginTime = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusCurrentTime = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel4 = new System.Windows.Forms.ToolStripStatusLabel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.mnsQuery.SuspendLayout();
            this.cmsRoomRightBtn.SuspendLayout();
            this.pnlDetails.SuspendLayout();
            this.pnlReserveDetails.SuspendLayout();
            this.ssr.SuspendLayout();
            this.SuspendLayout();
            // 
            // mnsQuery
            // 
            this.mnsQuery.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.mnsQuery.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.查询ToolStripMenuItem,
            this.管理ToolStripMenuItem,
            this.帮助ToolStripMenuItem});
            this.mnsQuery.Location = new System.Drawing.Point(0, 0);
            this.mnsQuery.Name = "mnsQuery";
            this.mnsQuery.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
            this.mnsQuery.Size = new System.Drawing.Size(1166, 25);
            this.mnsQuery.TabIndex = 0;
            this.mnsQuery.Text = "menuStrip1";
            // 
            // 查询ToolStripMenuItem
            // 
            this.查询ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.宾客查询ToolStripMenuItem,
            this.vIP查询ToolStripMenuItem,
            this.顾客历史查询ToolStripMenuItem});
            this.查询ToolStripMenuItem.Name = "查询ToolStripMenuItem";
            this.查询ToolStripMenuItem.Size = new System.Drawing.Size(44, 21);
            this.查询ToolStripMenuItem.Text = "查询";
            // 
            // 宾客查询ToolStripMenuItem
            // 
            this.宾客查询ToolStripMenuItem.Name = "宾客查询ToolStripMenuItem";
            this.宾客查询ToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.宾客查询ToolStripMenuItem.Text = "宾客查询";
            this.宾客查询ToolStripMenuItem.Click += new System.EventHandler(this.宾客查询ToolStripMenuItem_Click);
            // 
            // vIP查询ToolStripMenuItem
            // 
            this.vIP查询ToolStripMenuItem.Name = "vIP查询ToolStripMenuItem";
            this.vIP查询ToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.vIP查询ToolStripMenuItem.Text = "VIP查询";
            this.vIP查询ToolStripMenuItem.Click += new System.EventHandler(this.vIP查询ToolStripMenuItem_Click);
            // 
            // 顾客历史查询ToolStripMenuItem
            // 
            this.顾客历史查询ToolStripMenuItem.Name = "顾客历史查询ToolStripMenuItem";
            this.顾客历史查询ToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.顾客历史查询ToolStripMenuItem.Text = "顾客历史查询";
            this.顾客历史查询ToolStripMenuItem.Click += new System.EventHandler(this.顾客历史查询ToolStripMenuItem_Click);
            // 
            // 管理ToolStripMenuItem
            // 
            this.管理ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.房间管理ToolStripMenuItem,
            this.单价管理ToolStripMenuItem,
            this.vIP管理ToolStripMenuItem,
            this.用户管理ToolStripMenuItem});
            this.管理ToolStripMenuItem.Name = "管理ToolStripMenuItem";
            this.管理ToolStripMenuItem.Size = new System.Drawing.Size(44, 21);
            this.管理ToolStripMenuItem.Text = "管理";
            // 
            // 房间管理ToolStripMenuItem
            // 
            this.房间管理ToolStripMenuItem.Name = "房间管理ToolStripMenuItem";
            this.房间管理ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.房间管理ToolStripMenuItem.Text = "房间管理";
            this.房间管理ToolStripMenuItem.Click += new System.EventHandler(this.房间管理ToolStripMenuItem_Click);
            // 
            // 单价管理ToolStripMenuItem
            // 
            this.单价管理ToolStripMenuItem.Name = "单价管理ToolStripMenuItem";
            this.单价管理ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.单价管理ToolStripMenuItem.Text = "单价管理";
            this.单价管理ToolStripMenuItem.Click += new System.EventHandler(this.单价管理ToolStripMenuItem_Click);
            // 
            // vIP管理ToolStripMenuItem
            // 
            this.vIP管理ToolStripMenuItem.Name = "vIP管理ToolStripMenuItem";
            this.vIP管理ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.vIP管理ToolStripMenuItem.Text = "VIP管理";
            this.vIP管理ToolStripMenuItem.Click += new System.EventHandler(this.vIP管理ToolStripMenuItem_Click);
            // 
            // 用户管理ToolStripMenuItem
            // 
            this.用户管理ToolStripMenuItem.Name = "用户管理ToolStripMenuItem";
            this.用户管理ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.用户管理ToolStripMenuItem.Text = "用户管理";
            this.用户管理ToolStripMenuItem.Click += new System.EventHandler(this.用户管理ToolStripMenuItem_Click);
            // 
            // 帮助ToolStripMenuItem
            // 
            this.帮助ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.刷新ToolStripMenuItem,
            this.退出ToolStripMenuItem,
            this.关于ToolStripMenuItem});
            this.帮助ToolStripMenuItem.Name = "帮助ToolStripMenuItem";
            this.帮助ToolStripMenuItem.Size = new System.Drawing.Size(44, 21);
            this.帮助ToolStripMenuItem.Text = "帮助";
            // 
            // 刷新ToolStripMenuItem
            // 
            this.刷新ToolStripMenuItem.Name = "刷新ToolStripMenuItem";
            this.刷新ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.刷新ToolStripMenuItem.Text = "刷新界面";
            this.刷新ToolStripMenuItem.Click += new System.EventHandler(this.刷新ToolStripMenuItem_Click);
            // 
            // 退出ToolStripMenuItem
            // 
            this.退出ToolStripMenuItem.Name = "退出ToolStripMenuItem";
            this.退出ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.退出ToolStripMenuItem.Text = "退出系统";
            this.退出ToolStripMenuItem.Click += new System.EventHandler(this.退出ToolStripMenuItem_Click);
            // 
            // 关于ToolStripMenuItem
            // 
            this.关于ToolStripMenuItem.Name = "关于ToolStripMenuItem";
            this.关于ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.关于ToolStripMenuItem.Text = "关于";
            this.关于ToolStripMenuItem.Click += new System.EventHandler(this.关于ToolStripMenuItem_Click);
            // 
            // cmsRoomRightBtn
            // 
            this.cmsRoomRightBtn.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.cmsRoomRightBtn.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.开房ToolStripMenuItem,
            this.退房ToolStripMenuItem,
            this.打扫ToolStripMenuItem,
            this.结束打扫ToolStripMenuItem,
            this.锁定ToolStripMenuItem,
            this.结束锁定ToolStripMenuItem,
            this.禁用ToolStripMenuItem,
            this.结束禁用ToolStripMenuItem});
            this.cmsRoomRightBtn.Name = "cmsRoomRightBtn";
            this.cmsRoomRightBtn.Size = new System.Drawing.Size(125, 180);
            this.cmsRoomRightBtn.Opening += new System.ComponentModel.CancelEventHandler(this.CmsRoomRightBtn_Opening);
            // 
            // 开房ToolStripMenuItem
            // 
            this.开房ToolStripMenuItem.Name = "开房ToolStripMenuItem";
            this.开房ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.开房ToolStripMenuItem.Text = "开房";
            this.开房ToolStripMenuItem.Click += new System.EventHandler(this.开房ToolStripMenuItem_Click);
            // 
            // 退房ToolStripMenuItem
            // 
            this.退房ToolStripMenuItem.Name = "退房ToolStripMenuItem";
            this.退房ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.退房ToolStripMenuItem.Text = "退房";
            this.退房ToolStripMenuItem.Click += new System.EventHandler(this.退房ToolStripMenuItem_Click);
            // 
            // 打扫ToolStripMenuItem
            // 
            this.打扫ToolStripMenuItem.Name = "打扫ToolStripMenuItem";
            this.打扫ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.打扫ToolStripMenuItem.Text = "打扫";
            this.打扫ToolStripMenuItem.Click += new System.EventHandler(this.打扫ToolStripMenuItem_Click);
            // 
            // 结束打扫ToolStripMenuItem
            // 
            this.结束打扫ToolStripMenuItem.Name = "结束打扫ToolStripMenuItem";
            this.结束打扫ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.结束打扫ToolStripMenuItem.Text = "结束打扫";
            this.结束打扫ToolStripMenuItem.Click += new System.EventHandler(this.结束打扫ToolStripMenuItem_Click);
            // 
            // 锁定ToolStripMenuItem
            // 
            this.锁定ToolStripMenuItem.Name = "锁定ToolStripMenuItem";
            this.锁定ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.锁定ToolStripMenuItem.Text = "预定";
            this.锁定ToolStripMenuItem.Click += new System.EventHandler(this.预定ToolStripMenuItem_Click);
            // 
            // 结束锁定ToolStripMenuItem
            // 
            this.结束锁定ToolStripMenuItem.Name = "结束锁定ToolStripMenuItem";
            this.结束锁定ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.结束锁定ToolStripMenuItem.Text = "结束预定";
            this.结束锁定ToolStripMenuItem.Click += new System.EventHandler(this.结束锁定ToolStripMenuItem_Click);
            // 
            // 禁用ToolStripMenuItem
            // 
            this.禁用ToolStripMenuItem.Name = "禁用ToolStripMenuItem";
            this.禁用ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.禁用ToolStripMenuItem.Text = "禁用";
            this.禁用ToolStripMenuItem.Click += new System.EventHandler(this.禁用ToolStripMenuItem_Click);
            // 
            // 结束禁用ToolStripMenuItem
            // 
            this.结束禁用ToolStripMenuItem.Name = "结束禁用ToolStripMenuItem";
            this.结束禁用ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.结束禁用ToolStripMenuItem.Text = "结束禁用";
            this.结束禁用ToolStripMenuItem.Click += new System.EventHandler(this.结束禁用ToolStripMenuItem_Click);
            // 
            // pnlDetails
            // 
            this.pnlDetails.BackColor = System.Drawing.Color.Azure;
            this.pnlDetails.Controls.Add(this.lblRemarks);
            this.pnlDetails.Controls.Add(this.lblCustomerInfo);
            this.pnlDetails.Controls.Add(this.lblRoomState);
            this.pnlDetails.Controls.Add(this.lblUnitPrice);
            this.pnlDetails.Controls.Add(this.lblRoomType);
            this.pnlDetails.Controls.Add(this.lblFloor);
            this.pnlDetails.Controls.Add(this.lblRoomNum);
            this.pnlDetails.Controls.Add(this.label15);
            this.pnlDetails.Controls.Add(this.label11);
            this.pnlDetails.Controls.Add(this.label9);
            this.pnlDetails.Controls.Add(this.label13);
            this.pnlDetails.Controls.Add(this.label5);
            this.pnlDetails.Controls.Add(this.label7);
            this.pnlDetails.Controls.Add(this.label3);
            this.pnlDetails.Controls.Add(this.btnClose);
            this.pnlDetails.Controls.Add(this.label2);
            this.pnlDetails.Controls.Add(this.label1);
            this.pnlDetails.Location = new System.Drawing.Point(165, 100);
            this.pnlDetails.Name = "pnlDetails";
            this.pnlDetails.Size = new System.Drawing.Size(850, 400);
            this.pnlDetails.TabIndex = 2;
            this.pnlDetails.Visible = false;
            // 
            // lblRemarks
            // 
            this.lblRemarks.AutoSize = true;
            this.lblRemarks.Location = new System.Drawing.Point(148, 329);
            this.lblRemarks.Name = "lblRemarks";
            this.lblRemarks.Size = new System.Drawing.Size(17, 12);
            this.lblRemarks.TabIndex = 6;
            this.lblRemarks.Text = "无";
            // 
            // lblCustomerInfo
            // 
            this.lblCustomerInfo.AutoSize = true;
            this.lblCustomerInfo.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.lblCustomerInfo.Location = new System.Drawing.Point(148, 254);
            this.lblCustomerInfo.Name = "lblCustomerInfo";
            this.lblCustomerInfo.Size = new System.Drawing.Size(17, 12);
            this.lblCustomerInfo.TabIndex = 6;
            this.lblCustomerInfo.Text = "无";
            this.lblCustomerInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblRoomState
            // 
            this.lblRoomState.AutoSize = true;
            this.lblRoomState.Location = new System.Drawing.Point(148, 209);
            this.lblRoomState.Name = "lblRoomState";
            this.lblRoomState.Size = new System.Drawing.Size(29, 12);
            this.lblRoomState.TabIndex = 6;
            this.lblRoomState.Text = "空闲";
            // 
            // lblUnitPrice
            // 
            this.lblUnitPrice.AutoSize = true;
            this.lblUnitPrice.Location = new System.Drawing.Point(148, 168);
            this.lblUnitPrice.Name = "lblUnitPrice";
            this.lblUnitPrice.Size = new System.Drawing.Size(29, 12);
            this.lblUnitPrice.TabIndex = 6;
            this.lblUnitPrice.Text = "0/天";
            // 
            // lblRoomType
            // 
            this.lblRoomType.AutoSize = true;
            this.lblRoomType.Location = new System.Drawing.Point(148, 130);
            this.lblRoomType.Name = "lblRoomType";
            this.lblRoomType.Size = new System.Drawing.Size(41, 12);
            this.lblRoomType.TabIndex = 6;
            this.lblRoomType.Text = "单标间";
            // 
            // lblFloor
            // 
            this.lblFloor.AutoSize = true;
            this.lblFloor.Location = new System.Drawing.Point(148, 57);
            this.lblFloor.Name = "lblFloor";
            this.lblFloor.Size = new System.Drawing.Size(23, 12);
            this.lblFloor.TabIndex = 6;
            this.lblFloor.Text = "2楼";
            // 
            // lblRoomNum
            // 
            this.lblRoomNum.AutoSize = true;
            this.lblRoomNum.Location = new System.Drawing.Point(148, 95);
            this.lblRoomNum.Name = "lblRoomNum";
            this.lblRoomNum.Size = new System.Drawing.Size(23, 12);
            this.lblRoomNum.TabIndex = 6;
            this.lblRoomNum.Text = "2-1";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(63, 329);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(65, 12);
            this.label15.TabIndex = 6;
            this.label15.Text = "备注信息：";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(63, 254);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(65, 12);
            this.label11.TabIndex = 6;
            this.label11.Text = "顾客信息：";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(63, 209);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 12);
            this.label9.TabIndex = 6;
            this.label9.Text = "房间状态：";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(63, 168);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(65, 12);
            this.label13.TabIndex = 6;
            this.label13.Text = "当前单价：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(63, 130);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 12);
            this.label5.TabIndex = 6;
            this.label5.Text = "房间类型：";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(63, 57);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 12);
            this.label7.TabIndex = 6;
            this.label7.Text = "楼    层：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(63, 95);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 6;
            this.label3.Text = "房    号：";
            // 
            // btnClose
            // 
            this.btnClose.AutoSize = true;
            this.btnClose.BackColor = System.Drawing.Color.SkyBlue;
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("微软雅黑", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Location = new System.Drawing.Point(774, 0);
            this.btnClose.Name = "btnClose";
            this.btnClose.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnClose.Size = new System.Drawing.Size(75, 33);
            this.btnClose.TabIndex = 5;
            this.btnClose.Text = "关闭";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(4, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 12);
            this.label2.TabIndex = 4;
            this.label2.Text = "详细信息";
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.PaleTurquoise;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Location = new System.Drawing.Point(0, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(850, 1);
            this.label1.TabIndex = 3;
            // 
            // pnlReserveDetails
            // 
            this.pnlReserveDetails.BackColor = System.Drawing.Color.Azure;
            this.pnlReserveDetails.Controls.Add(this.lblReserveInfo);
            this.pnlReserveDetails.Controls.Add(this.lblRoomState2);
            this.pnlReserveDetails.Controls.Add(this.lblUnitPrice2);
            this.pnlReserveDetails.Controls.Add(this.lblRoomType2);
            this.pnlReserveDetails.Controls.Add(this.lblFloor2);
            this.pnlReserveDetails.Controls.Add(this.lblRoomNum2);
            this.pnlReserveDetails.Controls.Add(this.label18);
            this.pnlReserveDetails.Controls.Add(this.label19);
            this.pnlReserveDetails.Controls.Add(this.label20);
            this.pnlReserveDetails.Controls.Add(this.label21);
            this.pnlReserveDetails.Controls.Add(this.label22);
            this.pnlReserveDetails.Controls.Add(this.label23);
            this.pnlReserveDetails.Controls.Add(this.btnClose2);
            this.pnlReserveDetails.Controls.Add(this.label24);
            this.pnlReserveDetails.Controls.Add(this.label25);
            this.pnlReserveDetails.Location = new System.Drawing.Point(87, 111);
            this.pnlReserveDetails.Name = "pnlReserveDetails";
            this.pnlReserveDetails.Size = new System.Drawing.Size(988, 310);
            this.pnlReserveDetails.TabIndex = 5;
            this.pnlReserveDetails.Visible = false;
            // 
            // lblReserveInfo
            // 
            this.lblReserveInfo.AutoSize = true;
            this.lblReserveInfo.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.lblReserveInfo.Location = new System.Drawing.Point(148, 254);
            this.lblReserveInfo.Name = "lblReserveInfo";
            this.lblReserveInfo.Size = new System.Drawing.Size(17, 12);
            this.lblReserveInfo.TabIndex = 6;
            this.lblReserveInfo.Text = "无";
            this.lblReserveInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblRoomState2
            // 
            this.lblRoomState2.AutoSize = true;
            this.lblRoomState2.Location = new System.Drawing.Point(148, 209);
            this.lblRoomState2.Name = "lblRoomState2";
            this.lblRoomState2.Size = new System.Drawing.Size(29, 12);
            this.lblRoomState2.TabIndex = 6;
            this.lblRoomState2.Text = "空闲";
            // 
            // lblUnitPrice2
            // 
            this.lblUnitPrice2.AutoSize = true;
            this.lblUnitPrice2.Location = new System.Drawing.Point(148, 168);
            this.lblUnitPrice2.Name = "lblUnitPrice2";
            this.lblUnitPrice2.Size = new System.Drawing.Size(29, 12);
            this.lblUnitPrice2.TabIndex = 6;
            this.lblUnitPrice2.Text = "0/天";
            // 
            // lblRoomType2
            // 
            this.lblRoomType2.AutoSize = true;
            this.lblRoomType2.Location = new System.Drawing.Point(148, 130);
            this.lblRoomType2.Name = "lblRoomType2";
            this.lblRoomType2.Size = new System.Drawing.Size(41, 12);
            this.lblRoomType2.TabIndex = 6;
            this.lblRoomType2.Text = "单标间";
            // 
            // lblFloor2
            // 
            this.lblFloor2.AutoSize = true;
            this.lblFloor2.Location = new System.Drawing.Point(148, 57);
            this.lblFloor2.Name = "lblFloor2";
            this.lblFloor2.Size = new System.Drawing.Size(23, 12);
            this.lblFloor2.TabIndex = 6;
            this.lblFloor2.Text = "2楼";
            // 
            // lblRoomNum2
            // 
            this.lblRoomNum2.AutoSize = true;
            this.lblRoomNum2.Location = new System.Drawing.Point(148, 95);
            this.lblRoomNum2.Name = "lblRoomNum2";
            this.lblRoomNum2.Size = new System.Drawing.Size(23, 12);
            this.lblRoomNum2.TabIndex = 6;
            this.lblRoomNum2.Text = "2-1";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(63, 254);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(65, 12);
            this.label18.TabIndex = 6;
            this.label18.Text = "预定信息：";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(63, 209);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(65, 12);
            this.label19.TabIndex = 6;
            this.label19.Text = "房间状态：";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(63, 168);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(65, 12);
            this.label20.TabIndex = 6;
            this.label20.Text = "当前单价：";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(63, 130);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(65, 12);
            this.label21.TabIndex = 6;
            this.label21.Text = "房间类型：";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(63, 57);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(65, 12);
            this.label22.TabIndex = 6;
            this.label22.Text = "楼    层：";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(63, 95);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(65, 12);
            this.label23.TabIndex = 6;
            this.label23.Text = "房    号：";
            // 
            // btnClose2
            // 
            this.btnClose2.AutoSize = true;
            this.btnClose2.BackColor = System.Drawing.Color.SkyBlue;
            this.btnClose2.FlatAppearance.BorderSize = 0;
            this.btnClose2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose2.Font = new System.Drawing.Font("微软雅黑", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnClose2.ForeColor = System.Drawing.Color.White;
            this.btnClose2.Location = new System.Drawing.Point(913, 2);
            this.btnClose2.Name = "btnClose2";
            this.btnClose2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnClose2.Size = new System.Drawing.Size(75, 33);
            this.btnClose2.TabIndex = 5;
            this.btnClose2.Text = "关闭";
            this.btnClose2.UseVisualStyleBackColor = false;
            this.btnClose2.Click += new System.EventHandler(this.btnClose2_Click);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label24.Location = new System.Drawing.Point(4, 8);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(57, 12);
            this.label24.TabIndex = 4;
            this.label24.Text = "预定详情";
            // 
            // label25
            // 
            this.label25.BackColor = System.Drawing.Color.PaleTurquoise;
            this.label25.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label25.Location = new System.Drawing.Point(0, 30);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(988, 1);
            this.label25.TabIndex = 3;
            // 
            // ssr
            // 
            this.ssr.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.ssr.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusCurrentUser,
            this.toolStripStatusLoginTime,
            this.toolStripStatusCurrentTime,
            this.toolStripStatusLabel4});
            this.ssr.Location = new System.Drawing.Point(0, 589);
            this.ssr.Name = "ssr";
            this.ssr.Size = new System.Drawing.Size(1166, 22);
            this.ssr.TabIndex = 6;
            this.ssr.Text = "statusStrip1";
            // 
            // toolStripStatusCurrentUser
            // 
            this.toolStripStatusCurrentUser.Name = "toolStripStatusCurrentUser";
            this.toolStripStatusCurrentUser.Size = new System.Drawing.Size(68, 17);
            this.toolStripStatusCurrentUser.Text = "当前用户：";
            // 
            // toolStripStatusLoginTime
            // 
            this.toolStripStatusLoginTime.Name = "toolStripStatusLoginTime";
            this.toolStripStatusLoginTime.Size = new System.Drawing.Size(68, 17);
            this.toolStripStatusLoginTime.Text = "登录时间：";
            // 
            // toolStripStatusCurrentTime
            // 
            this.toolStripStatusCurrentTime.Name = "toolStripStatusCurrentTime";
            this.toolStripStatusCurrentTime.Size = new System.Drawing.Size(68, 17);
            this.toolStripStatusCurrentTime.Text = "现行时间：";
            // 
            // toolStripStatusLabel4
            // 
            this.toolStripStatusLabel4.Name = "toolStripStatusLabel4";
            this.toolStripStatusLabel4.Size = new System.Drawing.Size(84, 17);
            this.toolStripStatusLabel4.Text = "※听雨※  制作";
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1166, 611);
            this.Controls.Add(this.ssr);
            this.Controls.Add(this.pnlReserveDetails);
            this.Controls.Add(this.pnlDetails);
            this.Controls.Add(this.mnsQuery);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.mnsQuery;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.Name = "FrmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "酒店管理系统-主窗口";
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.mnsQuery.ResumeLayout(false);
            this.mnsQuery.PerformLayout();
            this.cmsRoomRightBtn.ResumeLayout(false);
            this.pnlDetails.ResumeLayout(false);
            this.pnlDetails.PerformLayout();
            this.pnlReserveDetails.ResumeLayout(false);
            this.pnlReserveDetails.PerformLayout();
            this.ssr.ResumeLayout(false);
            this.ssr.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip mnsQuery;
        private System.Windows.Forms.ToolStripMenuItem 查询ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 宾客查询ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vIP查询ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 房间管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 单价管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vIP管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 用户管理ToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip cmsRoomRightBtn;
        private System.Windows.Forms.ToolStripMenuItem 开房ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 退房ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 打扫ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 结束打扫ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 锁定ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 结束锁定ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 禁用ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 结束禁用ToolStripMenuItem;
        private System.Windows.Forms.Panel pnlDetails;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblRoomState;
        private System.Windows.Forms.Label lblRoomType;
        private System.Windows.Forms.Label lblFloor;
        private System.Windows.Forms.Label lblRoomNum;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblCustomerInfo;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lblRemarks;
        private System.Windows.Forms.Label lblUnitPrice;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ToolStripMenuItem 帮助ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 刷新ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 退出ToolStripMenuItem;
        private System.Windows.Forms.Panel pnlReserveDetails;
        private System.Windows.Forms.Label lblReserveInfo;
        private System.Windows.Forms.Label lblRoomState2;
        private System.Windows.Forms.Label lblUnitPrice2;
        private System.Windows.Forms.Label lblRoomType2;
        private System.Windows.Forms.Label lblFloor2;
        private System.Windows.Forms.Label lblRoomNum2;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Button btnClose2;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.ToolStripMenuItem 顾客历史查询ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 关于ToolStripMenuItem;
        private System.Windows.Forms.StatusStrip ssr;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusCurrentUser;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLoginTime;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusCurrentTime;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel4;
        private System.Windows.Forms.Timer timer1;
    }
}

