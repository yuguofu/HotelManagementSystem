﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DAL;
using Models;

namespace 酒店管理系统___试
{
    public partial class FrmLogin : Form
    {
        private DAL.UserManager userManager = new DAL.UserManager();
        public FrmLogin()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (txtUserName.Text.Trim().Length != 0 && txtUserName.Text.Trim().Length != 0)
            {
                Models.User user = new Models.User
                {
                    UserName = txtUserName.Text.Trim(),
                    Pwd = txtPwd.Text.Trim()
                };
                user = userManager.UserLogin(user);
                if (user != null)
                {
                    Program.currentUser = user;
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
                else
                {
                    MessageBox.Show("用户名或密码错误！", "提示");
                }
                
            }
            else
            {
                MessageBox.Show("账号、密码不能为空！");
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void txtUserName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                txtPwd.Focus();
            }
        }

        private void txtPwd_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                btnLogin_Click(sender, e);
            }
        }
    }
}
