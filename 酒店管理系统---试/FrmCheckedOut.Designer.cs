﻿namespace 酒店管理系统___试
{
    partial class FrmCheckedOut
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnClose = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.lblRemarks = new System.Windows.Forms.Label();
            this.lblCustomerInfo = new System.Windows.Forms.Label();
            this.lblUnitPrice = new System.Windows.Forms.Label();
            this.lblRoomType = new System.Windows.Forms.Label();
            this.lblFloor = new System.Windows.Forms.Label();
            this.lblRoomNum = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblBookedDay = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblDeposit = new System.Windows.Forms.Label();
            this.lblBookedTime = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lblPayable = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.lblVIP = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtPayAmount = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.lblRealityDay = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(928, 467);
            this.btnClose.Margin = new System.Windows.Forms.Padding(4);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(100, 29);
            this.btnClose.TabIndex = 51;
            this.btnClose.Text = "取消";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(776, 467);
            this.btnOK.Margin = new System.Windows.Forms.Padding(4);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(100, 29);
            this.btnOK.TabIndex = 50;
            this.btnOK.Text = "确认";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // lblRemarks
            // 
            this.lblRemarks.AutoSize = true;
            this.lblRemarks.Location = new System.Drawing.Point(166, 404);
            this.lblRemarks.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblRemarks.Name = "lblRemarks";
            this.lblRemarks.Size = new System.Drawing.Size(22, 15);
            this.lblRemarks.TabIndex = 52;
            this.lblRemarks.Text = "无";
            // 
            // lblCustomerInfo
            // 
            this.lblCustomerInfo.AutoSize = true;
            this.lblCustomerInfo.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.lblCustomerInfo.Location = new System.Drawing.Point(166, 220);
            this.lblCustomerInfo.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCustomerInfo.Name = "lblCustomerInfo";
            this.lblCustomerInfo.Size = new System.Drawing.Size(22, 15);
            this.lblCustomerInfo.TabIndex = 53;
            this.lblCustomerInfo.Text = "无";
            this.lblCustomerInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblUnitPrice
            // 
            this.lblUnitPrice.AutoSize = true;
            this.lblUnitPrice.Location = new System.Drawing.Point(166, 166);
            this.lblUnitPrice.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUnitPrice.Name = "lblUnitPrice";
            this.lblUnitPrice.Size = new System.Drawing.Size(38, 15);
            this.lblUnitPrice.TabIndex = 55;
            this.lblUnitPrice.Text = "0/天";
            // 
            // lblRoomType
            // 
            this.lblRoomType.AutoSize = true;
            this.lblRoomType.Location = new System.Drawing.Point(166, 118);
            this.lblRoomType.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblRoomType.Name = "lblRoomType";
            this.lblRoomType.Size = new System.Drawing.Size(52, 15);
            this.lblRoomType.TabIndex = 56;
            this.lblRoomType.Text = "单标间";
            // 
            // lblFloor
            // 
            this.lblFloor.AutoSize = true;
            this.lblFloor.Location = new System.Drawing.Point(166, 27);
            this.lblFloor.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFloor.Name = "lblFloor";
            this.lblFloor.Size = new System.Drawing.Size(30, 15);
            this.lblFloor.TabIndex = 57;
            this.lblFloor.Text = "2楼";
            // 
            // lblRoomNum
            // 
            this.lblRoomNum.AutoSize = true;
            this.lblRoomNum.Location = new System.Drawing.Point(166, 75);
            this.lblRoomNum.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblRoomNum.Name = "lblRoomNum";
            this.lblRoomNum.Size = new System.Drawing.Size(31, 15);
            this.lblRoomNum.TabIndex = 58;
            this.lblRoomNum.Text = "2-1";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(53, 404);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(82, 15);
            this.label8.TabIndex = 59;
            this.label8.Text = "备注信息：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(53, 220);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 15);
            this.label5.TabIndex = 60;
            this.label5.Text = "顾客信息：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(53, 166);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 15);
            this.label4.TabIndex = 62;
            this.label4.Text = "当前单价：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(53, 118);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 15);
            this.label3.TabIndex = 63;
            this.label3.Text = "房间类型：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(53, 27);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 15);
            this.label1.TabIndex = 64;
            this.label1.Text = "楼    层：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(53, 75);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 15);
            this.label2.TabIndex = 65;
            this.label2.Text = "房    号：";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(381, 75);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(82, 15);
            this.label6.TabIndex = 59;
            this.label6.Text = "预住天数：";
            // 
            // lblBookedDay
            // 
            this.lblBookedDay.AutoSize = true;
            this.lblBookedDay.Location = new System.Drawing.Point(494, 75);
            this.lblBookedDay.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblBookedDay.Name = "lblBookedDay";
            this.lblBookedDay.Size = new System.Drawing.Size(15, 15);
            this.lblBookedDay.TabIndex = 52;
            this.lblBookedDay.Text = "0";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(381, 123);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(84, 15);
            this.label7.TabIndex = 59;
            this.label7.Text = "押    金：";
            // 
            // lblDeposit
            // 
            this.lblDeposit.AutoSize = true;
            this.lblDeposit.Location = new System.Drawing.Point(494, 123);
            this.lblDeposit.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDeposit.Name = "lblDeposit";
            this.lblDeposit.Size = new System.Drawing.Size(15, 15);
            this.lblDeposit.TabIndex = 52;
            this.lblDeposit.Text = "0";
            // 
            // lblBookedTime
            // 
            this.lblBookedTime.AutoSize = true;
            this.lblBookedTime.Location = new System.Drawing.Point(494, 27);
            this.lblBookedTime.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblBookedTime.Name = "lblBookedTime";
            this.lblBookedTime.Size = new System.Drawing.Size(37, 15);
            this.lblBookedTime.TabIndex = 66;
            this.lblBookedTime.Text = "时间";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(381, 27);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(82, 15);
            this.label9.TabIndex = 67;
            this.label9.Text = "入住时间：";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(794, 75);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(82, 15);
            this.label11.TabIndex = 59;
            this.label11.Text = "应付金额：";
            // 
            // lblPayable
            // 
            this.lblPayable.AutoSize = true;
            this.lblPayable.Location = new System.Drawing.Point(907, 75);
            this.lblPayable.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPayable.Name = "lblPayable";
            this.lblPayable.Size = new System.Drawing.Size(15, 15);
            this.lblPayable.TabIndex = 52;
            this.lblPayable.Text = "0";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(381, 166);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(91, 15);
            this.label13.TabIndex = 59;
            this.label13.Text = "是否有VIP：";
            // 
            // lblVIP
            // 
            this.lblVIP.AutoSize = true;
            this.lblVIP.Location = new System.Drawing.Point(494, 166);
            this.lblVIP.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblVIP.Name = "lblVIP";
            this.lblVIP.Size = new System.Drawing.Size(22, 15);
            this.lblVIP.TabIndex = 52;
            this.lblVIP.Text = "否";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(794, 123);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(82, 15);
            this.label15.TabIndex = 59;
            this.label15.Text = "实付金额：";
            // 
            // txtPayAmount
            // 
            this.txtPayAmount.Location = new System.Drawing.Point(908, 116);
            this.txtPayAmount.Name = "txtPayAmount";
            this.txtPayAmount.Size = new System.Drawing.Size(118, 25);
            this.txtPayAmount.TabIndex = 68;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(794, 27);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(82, 15);
            this.label10.TabIndex = 59;
            this.label10.Text = "实住天数：";
            // 
            // lblRealityDay
            // 
            this.lblRealityDay.AutoSize = true;
            this.lblRealityDay.Location = new System.Drawing.Point(907, 27);
            this.lblRealityDay.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblRealityDay.Name = "lblRealityDay";
            this.lblRealityDay.Size = new System.Drawing.Size(15, 15);
            this.lblRealityDay.TabIndex = 52;
            this.lblRealityDay.Text = "0";
            // 
            // FrmCheckedOut
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1047, 509);
            this.Controls.Add(this.txtPayAmount);
            this.Controls.Add(this.lblBookedTime);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.lblPayable);
            this.Controls.Add(this.lblVIP);
            this.Controls.Add(this.lblDeposit);
            this.Controls.Add(this.lblRealityDay);
            this.Controls.Add(this.lblBookedDay);
            this.Controls.Add(this.lblRemarks);
            this.Controls.Add(this.lblCustomerInfo);
            this.Controls.Add(this.lblUnitPrice);
            this.Controls.Add(this.lblRoomType);
            this.Controls.Add(this.lblFloor);
            this.Controls.Add(this.lblRoomNum);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnOK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmCheckedOut";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "退房";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmCheckedOut_FormClosed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Label lblRemarks;
        private System.Windows.Forms.Label lblCustomerInfo;
        private System.Windows.Forms.Label lblUnitPrice;
        private System.Windows.Forms.Label lblRoomType;
        private System.Windows.Forms.Label lblFloor;
        private System.Windows.Forms.Label lblRoomNum;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblBookedDay;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblDeposit;
        private System.Windows.Forms.Label lblBookedTime;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lblPayable;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label lblVIP;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtPayAmount;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblRealityDay;
    }
}