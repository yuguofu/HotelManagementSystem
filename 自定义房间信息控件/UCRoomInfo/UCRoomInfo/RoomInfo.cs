﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UCRoomInfo
{
    public partial class RoomInfo: UserControl
    {
        public RoomInfo()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 自定义控件带参数构造方法（房间信息）
        /// </summary>
        /// <param name="roomNumber">房间号</param>
        /// <param name="roomType">房间类型</param>
        /// <param name="roomState">房间状态</param>
        public RoomInfo(string roomNumber, string roomType, string roomState)
        {
            InitializeComponent();
            lblRoomNumber.Text = roomNumber;
            lblRoomType.Text = "类型:" + roomType;
            lblRoomStateName.Text =  roomState;
        }


        #region 属性
        [Category("数据"), Description("房间编号"), Browsable(true)]
        public string RoomNumber 
        {
            get { return lblRoomNumber.Text; }
            set { lblRoomNumber.Text = value; }
        }
        [Category("数据"), Description("房间类型ID"), Browsable(true)]
        public string RoomTypeID
        {
            get { return lblTypeName.Tag.ToString(); }
            set { lblTypeName.Tag = value; }
        }
        [Category("数据"), Description("房间类型"), Browsable(true)]
        public string RoomType
        {
            get { return lblTypeName.Text; }
            set { lblTypeName.Text = value; }
        }
        [Category("数据"), Description("单价"), Browsable(true)]
        public string RoomUnitPrice
        {
            get { return lblUnitPrice.Text; }
            set { lblUnitPrice.Text = value; }
        }
        [Category("数据"), Description("房间状态"), Browsable(true)]
        public string RoomState
        {
            get { return lblRoomStateName.Text; }
            set { lblRoomStateName.Text = value; }
        }
        [Category("数据"), Description("房间状态ID"), Browsable(true)]
        public string RoomStateID
        {
            get { return lblRoomStateName.Tag.ToString(); }
            set { lblRoomStateName.Tag = value; }
        }
        #endregion
    }
}
