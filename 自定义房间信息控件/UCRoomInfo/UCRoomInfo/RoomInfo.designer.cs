﻿namespace UCRoomInfo
{
    partial class RoomInfo
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.lblRoomNumber = new System.Windows.Forms.Label();
            this.lblRoomState = new System.Windows.Forms.Label();
            this.lblRoomType = new System.Windows.Forms.Label();
            this.lblRoomStateName = new System.Windows.Forms.Label();
            this.lblTypeName = new System.Windows.Forms.Label();
            this.lblUnitPrice = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblRoomNumber
            // 
            this.lblRoomNumber.BackColor = System.Drawing.Color.SkyBlue;
            this.lblRoomNumber.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblRoomNumber.Font = new System.Drawing.Font("微软雅黑", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblRoomNumber.Location = new System.Drawing.Point(0, 0);
            this.lblRoomNumber.Margin = new System.Windows.Forms.Padding(0);
            this.lblRoomNumber.Name = "lblRoomNumber";
            this.lblRoomNumber.Size = new System.Drawing.Size(98, 24);
            this.lblRoomNumber.TabIndex = 0;
            this.lblRoomNumber.Text = "房号";
            this.lblRoomNumber.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblRoomState
            // 
            this.lblRoomState.AutoSize = true;
            this.lblRoomState.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblRoomState.Font = new System.Drawing.Font("楷体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblRoomState.Location = new System.Drawing.Point(17, 97);
            this.lblRoomState.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblRoomState.Name = "lblRoomState";
            this.lblRoomState.Size = new System.Drawing.Size(38, 12);
            this.lblRoomState.TabIndex = 1;
            this.lblRoomState.Text = "状态:";
            this.lblRoomState.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblRoomType
            // 
            this.lblRoomType.AutoSize = true;
            this.lblRoomType.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblRoomType.Font = new System.Drawing.Font("仿宋", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblRoomType.Location = new System.Drawing.Point(17, 34);
            this.lblRoomType.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblRoomType.Name = "lblRoomType";
            this.lblRoomType.Size = new System.Drawing.Size(38, 12);
            this.lblRoomType.TabIndex = 1;
            this.lblRoomType.Text = "类型:";
            this.lblRoomType.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblRoomStateName
            // 
            this.lblRoomStateName.AutoSize = true;
            this.lblRoomStateName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblRoomStateName.Font = new System.Drawing.Font("楷体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblRoomStateName.ForeColor = System.Drawing.Color.Blue;
            this.lblRoomStateName.Location = new System.Drawing.Point(49, 97);
            this.lblRoomStateName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblRoomStateName.Name = "lblRoomStateName";
            this.lblRoomStateName.Size = new System.Drawing.Size(31, 12);
            this.lblRoomStateName.TabIndex = 2;
            this.lblRoomStateName.Tag = "1";
            this.lblRoomStateName.Text = "空闲";
            this.lblRoomStateName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTypeName
            // 
            this.lblTypeName.AutoSize = true;
            this.lblTypeName.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblTypeName.Font = new System.Drawing.Font("楷体", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lblTypeName.ForeColor = System.Drawing.Color.DarkOrchid;
            this.lblTypeName.Location = new System.Drawing.Point(49, 34);
            this.lblTypeName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTypeName.Name = "lblTypeName";
            this.lblTypeName.Size = new System.Drawing.Size(31, 12);
            this.lblTypeName.TabIndex = 3;
            this.lblTypeName.Tag = "1";
            this.lblTypeName.Text = "单标";
            this.lblTypeName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblUnitPrice
            // 
            this.lblUnitPrice.AutoSize = true;
            this.lblUnitPrice.Location = new System.Drawing.Point(34, 107);
            this.lblUnitPrice.Name = "lblUnitPrice";
            this.lblUnitPrice.Size = new System.Drawing.Size(29, 12);
            this.lblUnitPrice.TabIndex = 4;
            this.lblUnitPrice.Text = "单价";
            this.lblUnitPrice.Visible = false;
            // 
            // RoomInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.PaleTurquoise;
            this.Controls.Add(this.lblUnitPrice);
            this.Controls.Add(this.lblTypeName);
            this.Controls.Add(this.lblRoomType);
            this.Controls.Add(this.lblRoomStateName);
            this.Controls.Add(this.lblRoomState);
            this.Controls.Add(this.lblRoomNumber);
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "RoomInfo";
            this.Size = new System.Drawing.Size(98, 120);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblRoomNumber;
        private System.Windows.Forms.Label lblRoomState;
        private System.Windows.Forms.Label lblRoomType;
        private System.Windows.Forms.Label lblRoomStateName;
        private System.Windows.Forms.Label lblTypeName;
        private System.Windows.Forms.Label lblUnitPrice;
    }
}
